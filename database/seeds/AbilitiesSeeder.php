<?php

use Illuminate\Database\Seeder;

class AbilitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('abilities')->insert([
            'name' => 'admin_properties',
            'label' => null,
        ]);
        DB::table('abilities')->insert([
            'name' => 'teacher_properties',
            'label' => null,
        ]);
        DB::table('abilities')->insert([
            'name' => 'student_properties',
            'label' => null,
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // factory(App\User::class,25)->create();

        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('qwerty'),            
        ]);

        DB::table('users')->insert([
            'name' => 'teacher1',
            'email' => 'teacher1@gmail.com',
            'password' => Hash::make('qwerty'),
            'teacher_group_id' => 1,            
        ]);

        DB::table('users')->insert([
            'name' => 'student1',
            'email' => 'student1@gmail.com',
            'password' => Hash::make('qwerty'),
            'student_group_id' => 1,            
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class AbilityRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ability_role')->insert([
            'role_id' => 1,
            'ability_id' => 1,
        ]);
        DB::table('ability_role')->insert([
            'role_id' => 2,
            'ability_id' => 2,
        ]);
        DB::table('ability_role')->insert([
            'role_id' => 3,
            'ability_id' => 3,
        ]);
    }
}

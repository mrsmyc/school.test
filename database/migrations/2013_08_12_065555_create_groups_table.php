<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->id();
            $table->string('name'); 
            // $table->foreignId('teacher_id')->constrained('users')->onDelete('cascade');
            // $table->integer('students_count')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::dropIfExists('groups');
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('groups');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}

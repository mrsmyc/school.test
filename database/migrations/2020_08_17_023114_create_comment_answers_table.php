<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comment_answers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('comment_id')->constrained('task_comments')->onDelete('cascade');
            $table->foreignId('teacher_id')->constrained('users')->onDelete('restrict');
            $table->text('answer');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::dropIfExists('comment_answer');
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::drop('comment_answers');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}

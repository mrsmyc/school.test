<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// })->name('home');

Route::get('/', 'SessionController@userRedirect')->name('home');

Route::get('/login', 'SessionController@create')->name('login');

Route::post('/login', 'SessionController@store');

Route::get('/logout', 'SessionController@destroy');

Route::get('/register', 'RegistrationController@create');

Route::post('/register','RegistrationController@store');

Route::get('/admin', 'AdminController@index')->name('adminPanel');

Route::get('/admin/groups', 'GroupController@index')->name('showGroups');

Route::get('/admin/groups/{id}/redact', 'GroupController@edit');

Route::post('/admin/groups/{id}/redact', 'GroupController@update');

Route::get('/admin/groups/create', 'GroupController@create');

Route::post('/admin/groups/create', 'GroupController@store');

Route::get('/admin/groups/{id}/delete', 'GroupController@delete');

Route::get('/admin/groups/show/{id}', 'GroupController@show')->name('showGroup');

Route::get('/admin/groups/{id}/teacher/change', 'AdminController@changeTeacher');

Route::post('/admin/groups/{id}/teacher/change', 'AdminController@changeTeacherStore');

Route::get('/admin/groups/show/{id}/tasks/create', 'AdminController@createTask');

Route::post('/admin/groups/show/{id}/tasks/create', 'AdminController@storeTask');

Route::get('/admin/task/{id}/delete', 'AdminController@deleteTask');

Route::get('/admin/teachers', 'AdminController@indexTeachers')->name('showTeachers');

Route::get('/admin/teachers/create', 'AdminController@createTeacher');

Route::post('/admin/teachers/create', 'AdminController@storeTeacher');

Route::get('/admin/students', 'AdminController@indexStudent')->name('showStudents');

Route::get('/admin/students/create', 'AdminController@createStudent');

Route::post('/admin/students/create', 'AdminController@storeStudent');

Route::get('/admin/students/{id}/enroll', 'AdminController@studentGroupEnroll');

Route::post('/admin/students/{id}/enroll', 'AdminController@studentGroupStore');

Route::get('/admin/students/{id}', 'AdminController@showStudent')->name('studentSolution');

Route::get('/admin/students/{studentId}/solution/{solutionId}/redact', 'AdminController@editSolution');

Route::get('/admin/students/{studentId}/solution/{solutionId}/delete', 'AdminController@deleteSolution');

Route::get('/admin/task/{taskId}/solutions/{solutionId}/redact','AdminController@editTaskSolution');

Route::post('/admin/task/{taskId}/solutions/{solutionId}/redact','AdminController@updateTaskSolution');

Route::post('/admin/students/{studentId}/solution/{solutionId}/redact', 'AdminController@updateSolution');

Route::get('/admin/students/{id}/questions', 'AdminController@showQuestions')->name('showAdminQuestions');

Route::get('/admin/students/{studentId}/questions/{questionId}/delete', 'AdminController@deleteQuestion');

Route::get('/admin/students/{studentId}/questions/{questionId}', 'AdminController@editQuestion');

Route::post('/admin/students/{studentId}/questions/{questionId}', 'AdminController@updateQuestion');

Route::get('/admin/task/{id}', 'AdminController@showTask')->name('showTaskAdmin');

Route::get('/admin/task/{taskId}/comment/{commentId}/delete', 'AdminController@deleteComment');

Route::get('/admin/task/{id}/edit', 'AdminController@editTask');

Route::post('/admin/task/{id}/edit', 'AdminController@updateTask');

Route::get('/admin/task/{taskId}/comment/edit/{commentId}', 'AdminController@editComment');

Route::post('/admin/task/{taskId}/comment/edit/{commentId}', 'AdminController@updateComment');

Route::get('/admin/reports', 'AdminController@reports')->name('showAdminReports');

Route::get('/admin/reports/{id}/delete', 'AdminController@deleteReport');

Route::get('/admin/reports/{id}/answer', 'AdminController@createReportAnswer');

Route::post('/admin/reports/{id}/answer', 'AdminController@storeReportAnswer');

Route::get('/admin/reports/{reportId}/answer/{answerId}/redact', 'AdminController@editReportAnswer');

Route::post('/admin/reports/{reportId}/answer/{answerId}/redact', 'AdminController@updateReportAnswer');

Route::get('/admin/task/{id}/solutions', 'AdminController@watchTaskSolution')->name('watchTaskSolution');

Route::get('/student/tasks', 'StudentController@index')->name('studentPanel');

Route::get('/student/task/{id}/solution/add', 'StudentController@createSolution');

Route::post('/student/task/{id}/solution/add', 'StudentController@storeSolution');

Route::get('/student/task/{id}', 'TaskController@showTask')->name('showTaskStudent');

Route::get('/student/task/{studentId}/solution/{solutionId}/edit', 'StudentController@editSolution');

Route::post('/student/task/{studentId}/solution/{solutionId}/edit', 'StudentController@updateSolution');

Route::post('/student/task/{id}', 'TaskController@storeTaskComment');

Route::get('/student/marks', 'StudentController@showMarks');

Route::get('/student/reports', 'ReportController@showReports')->name('showStudentReports');

Route::get('/student/reports/create', 'ReportController@createReport');

Route::post('/student/reports/create', 'ReportController@storeReport');

Route::get('/student/task/{id}/question', 'StudentController@createQuestion');

Route::post('/student/task/{id}/question', 'StudentController@storeQuestion');

Route::get('/teacher/group', 'TeacherController@index')->name('teacherPanel');

Route::get('/teacher/group/{id}/tasks', 'TeacherController@tasks')->name('teacherTasks');

Route::get('/teacher/group/{groupId}/tasks/{taskId}/redact', 'TeacherController@editTask');

Route::post('/teacher/group/{groupId}/tasks/{taskId}/redact', 'TeacherController@updateTask');

Route::get('/teacher/group/{id}/tasks/create', 'TaskController@create');

Route::post('/teacher/group/{id}/tasks/create', 'TaskController@store');

Route::get('/teacher/task/{id}/answers', 'TeacherController@showAnswers')->name('showAnswers');

Route::post('/teacher/task/{id}/answers/rating', 'TeacherController@setRating');

Route::get('/teacher/group/student/{id}/marks', 'TeacherController@showMarks');

Route::get('/teacher/task/{id}', 'TeacherController@showTask')->name('showTaskTeacher');

Route::post('/teacher/task/{id}', 'TeacherController@answerComment');

Route::get('/teacher/questions', 'TeacherController@showQuestions')->name('showQuestions');

Route::post('/teacher/questions/{id}', 'TeacherController@storeAnswer');

Route::get('/teacher/reports', 'ReportController@showReports')->name('showTeacherReports');

Route::get('/teacher/reports/create', 'ReportController@createReport');

Route::post('/teacher/reports/create', 'ReportController@storeReport');

////////задание на ecxel

Route::get('/test/1task', 'TestController@firstTask');
Route::post('/test/1task', 'TestController@firstTaskStore');
Route::get('/test/2task', 'TestController@secondTask');
Route::post('/test/2task', 'TestController@secondTaskStore');
Route::get('/test/3task', 'TestController@thirdTask');
Route::post('/test/3task', 'TestController@letterShift');
Route::get('/test/4task', 'TestController@fourthTask');
Route::get('/test/4task', 'TestController@fourthTask');
Route::post('/test/4task', 'TestController@priceRound');
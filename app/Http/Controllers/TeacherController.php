<?php

namespace App\Http\Controllers;

use App\User;
use App\Task;
use App\TaskComment;
use App\Group;
use App\Solution;
use App\CommentAnswer;
use App\Question;
use App\QuestionAnswer;
use App\Report;
use App\ReportAnswer;
use App\Events\RatingEvent;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Mail;
use App\Mail\SolutionChecked;

use Illuminate\Http\Request;

class TeacherController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('can:admin_properties'||'can:teacher_properties');
    }

    public function index() {

        $group = Group::where('id', Auth::user()->teacher_group_id)->first();
        
        if($group){
            $tasks = Task::where('group_id', $group->id)->get();

            $students = User::where('student_group_id', $group->id)->paginate(1);
            return view('teacher_panel.index', compact('group', 'students', 'tasks'));
        }else {
            abort(404);
        }
        
    }

    public function tasks($id) {

        $tasks = Task::where('group_id', $id)->paginate(1);
        $group = Group::where('id', $id)->first();
        $students = User::where('student_group_id', $group->id)->get();
        foreach($tasks as $task) {
            $count = 0;
            foreach($task->solutions as $solution) {
                if($solution->rating != null && $solution->teacher_answer != null) {
                    $count++;
                }
            $task['completed'] = $count;
            }            
        }
        if(Auth::user()->teacher_group_id != $group->id) {
            abort(404);
        }else {
            return view('teacher_panel.tasks', compact('tasks', 'group', 'students'));
        }

    }

    public function showAnswers($id) {

        $solutions = Solution::where('task_id', $id)->paginate(1);
        $task = Task::where('id', $id)->first();
        if($task == null) {
            abort(404);
        }
        if($task->group_id != Auth::user()->teacher_group_id){
            abort(404);
        }
        $group = Group::where('id', Auth::user()->groupAsTeacher->id)->first();

        if(Auth::user()->roles->first()->name == 'teacher') {
            return view('teacher_panel.showSolutions', compact('solutions', 'task', 'group'));
        }else {
            return view();
        }
    }

    public function setRating(Request $request, $id) {
        $this->validate(request(), [

            'comment' => 'required',

            'rating' => 'required',
        ], [
            'comment.required' => 'Введите комментарий к оценке',

            'rating.required' => 'Введите оценку от 1 до 5'
        ]);
        $solution = Solution::where('id', $id)->first();
        $solution->rating = $request->rating;
        $solution->teacher_answer = $request->comment;
        $solution->save();
        $student = User::find($solution->student_id);
        $solutions = Solution::where('student_id', $student->id)->get();
        $marksCount = 0;
        $marksSumm = 0;
        foreach($solutions as $solution) {
            if($solution->teacher_answer != null && $solution->rating !=null) {
                $marksCount++;
                $marksSumm = $marksSumm + $solution->rating;
            }
        }
        if($marksCount != 0 && $marksSumm != 0) {
            $student->student_rating = $marksSumm / $marksCount;
            $student->save();
        }
        $task = Task::where('id', $solution->task_id)->first();
        $teacher = Auth::user();
        //$data = array($task,$solution,$teacher);
        Mail::to($student)->send(new SolutionChecked($teacher, $solution, $task));

        return redirect()->route('showAnswers', $request->groupId);
    }

    public function editTask(Request $request, $groupId, $taskId) {
        $task = Task::find($taskId);
        $group = Group::find($groupId);
        if($groupId != Auth::user()->teacher_group_id) {
            abort(404);
        }

        return view('teacher_panel.editTask', compact('task', 'group'));
    }

    public function updateTask(Request $request, $groupId, $taskId) {
        $this->validate(request(), [
            'title' => 'required',

            'task' => 'required',

            'date' => 'required'
        ], [
            'title.required' => 'Введите предмет задания',

            'task.required' => 'Введите задание',

            'date.required' => 'Выберите дату'
        ]);
        $task = Task::find($taskId);
        $task->title = $request->title;
        $task->task = $request->task;
        $task->date = $request->date;
        $task->save();
        $group = Group::find($groupId);

        return redirect()->route('teacherTasks', $groupId)->with(['group' => $group]);
    }

    public function showMarks($id) {
        $student = User::where('id', $id)->first();
        $group = Group::where('id', $student->student_group_id)->first();
        if($student->student_group_id != Auth::user()->teacher_group_id) {
            abort(404);
        }
        $solutions = Solution::where('student_id', $id)->paginate(1);

        return view('teacher_panel.showMarks', compact('solutions', 'student', 'group'));
    }

    public function showTask($id) {
        $task = Task::where('id', $id)->first();
        if($task->group_id != Auth::user()->teacher_group_id) {
            abort(404);
        }
        $group = Group::find($task->group_id);
        $comments = TaskComment::where('task_id', $task->id)->paginate(1);    

        return view('teacher_panel.showTask', compact('task', 'comments', 'group'));
    }

    public function answerComment(Request $request, $id) {
        $this->validate(request(), [

            'answer' => 'required'

        ], [
            'answer.required' => 'Введите ответ'
        ]);
        $answer = new CommentAnswer();
        $answer->comment_id = $id;
        $answer->teacher_id = Auth::user()->id;
        $answer->answer = $request->answer;
        $answer->save();

        return redirect()->route('showTaskTeacher', $request->taskId);
    }

    public function showQuestions() {
        $group = Group::where('id', Auth::user()->teacher_group_id)->first();
        $questions = Question::whereHas('student', function(Builder $query) use ($group){
            $query->where('student_group_id', 'like', $group->id);
        })->paginate(1);

        return view('teacher_panel.showQuestions', compact('questions', 'group'));
    }

    public function storeAnswer(Request $request, $id) {
        $this->validate(request(), [
            'text' => 'required'
        ], [
            'text.required' => 'Введите ответ'
        ]);
        $answer = new QuestionAnswer();
        $answer->question_id = $id;
        $answer->teacher_id = Auth::user()->id;
        $answer->text = $request->text;
        $answer->save();

        return redirect()->route('showQuestions');
    }
}

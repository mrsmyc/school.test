<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Group;
use App\Task;
use Illuminate\Support\Facades\Auth;
use App\Solution;
use App\QuestionAnswer;
use App\Question;
use App\Report;
use App\ReportAnswer;
use App\Mail\TaskCompleted;
use App\Mail\QuestionStored;
use Illuminate\Support\Facades\Mail;

class StudentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('can:admin_properties'||'can:student_properties');
    }


    public function index() {
        $student = Auth::user();
        $group = Group::where('id', $student->student_group_id)->first();
        $tasks = Task::where('group_id', $group->id)->paginate(1);
        $solutions = Solution::where('student_id', $student->id)->get();
        // dd($solutions);
        return view('student_panel.index', compact('group', 'student', 'tasks', 'solutions'));        
    }

    public function createSolution($id) {
        $task = Task::where('id', $id)->first();

        return view('student_panel.solution', compact('task'));
    }

    public function storeSolution(Request $request) {
        $this->validate(request(), [

            'text' => 'required'
        ], [
            'text.required' => 'Введите решение'
        ]);
        $solution = new Solution();
        $solution->task_id = $request->taskId;
        $solution->student_id = Auth::user()->id;
        $solution->text = $request->text;
        
        $solution->save();
        $teacher = User::where('teacher_group_id', Auth::user()->student_group_id)->first();
        $student = Auth::user();
        $task = Task::find($request->taskId);
        Mail::to($teacher)->send(new TaskCompleted($student,$task));

        return redirect()->route('studentPanel');

    }

    public function showMarks() {
        $student = Auth::user();        
        $solutions = Solution::where([
            ['student_id', '=', $student->id],
            ['rating', '!=', 0],
        ])->paginate(1);
        return view('student_panel.showMarks', compact('student', 'solutions'));
    }

    public function createQuestion($id) {
        $question = Question::where([
            ['task_id', '=', $id],
            ['student_id', '=', Auth::user()->id],
        ])->first();
        $task = Task::find($id);
        if($question) {
            $answer = QuestionAnswer::where('question_id', $question->id)->first();
            return view('student_panel.showQuestion', compact('question', 'answer', 'task'));
        }else {
            return view('student_panel.createQuestion', compact('task'));
        }
    }

    public function storeQuestion(Request $request) {
        $this->validate(request(), [
            'text' => 'required'
        ], [
            'text.required' => 'Введите вопрос'
        ]);

        $question = new Question();
        $question->task_id = $request->taskId;
        $question->student_id = Auth::user()->id;
        $question->text = $request->text;
        $question->save();
        $student = Auth::user();
        $teacher = User::where('teacher_group_id', $student->student_group_id)->first();
        Mail::to($teacher)->send(new QuestionStored($question,$student));
        
        return redirect()->route('studentPanel');
    }

    public function editSolution($studentId, $solutionId) {
        $solution = Solution::find($solutionId);
        if($solution->student_id != Auth::user()->id) {
            abort(404);
        }
        return view('student_panel.editSolution', compact('solution'));
    }
    
    public function updateSolution(Request $request, $studentId, $solutionId) {
        $this->validate(request(), [
            'text' => 'required'

        ], [
            'text.required' => 'Введите решение'
        ]);
        $solution = Solution::find($solutionId);
        
        if($solution->rating != null && $solution->teacher_answer != null) {
            abort(404);
        }else {
            $solution->text = $request->text;
            $solution->save();
        }
        
        return redirect()->route('showTaskStudent', $solution->task->id);
    }
}

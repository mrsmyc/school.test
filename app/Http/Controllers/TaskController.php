<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use App\User;
use App\Group;
use App\Task;
use App\TaskComment;
use App\Solution;
use Illuminate\Support\Facades\Mail;
use App\Mail\TaskCreated;

class TaskController extends Controller
{    
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('can:admin_properties'||'can:teacher_properties')->except([
            'showTask',
        ]);
    }

    public function create($id) {
        
        // $students = User::where('student_group_id', $id)->get();
        $group = Group::where('id', $id)->first();
        if($id != Auth::user()->teacher_group_id) {
            abort(404);
        }

        return view('task.create', compact('group'));

    }

    public function store(Request $request) {
        
        $students = User::where('student_group_id', $request->groupId)->get();
        $teacher = User::where('teacher_group_id', $request->groupId)->first();
        $this->validate(request(), [

            'task' => 'required',

            'title' => 'required',

            'date' => 'required|date'

        ], [
            'title.required' => 'Введите предмет задания',

            'task.required' => 'Введите задание',
            
            'date.required' => 'Выберите дату'
        ]);
        $task = new Task();
        $task->group_id = $request->groupId;
        $task->task = $request->task;
        $task->title = $request->title;
        $task->deadline = $request->date;
        $task->save();
        
        $author = Auth::user();
        $usersEmails = User::where('student_group_id', $request->groupId)->get();
        foreach($usersEmails as $user) {
            Mail::to($user)->send(new TaskCreated($task, $author));
        }
    
        if(Auth::user()->roles->first()->name == 'teacher') {
            return redirect()->route('teacherTasks',$request->groupId);
        }else {
            return redirect()->route('showGroup',$request->groupId);
        }

    }

    public function showTask($id) {
        $task = Task::where('id', $id)->first();
        if(Auth::user()->student_group_id != $task->group_id) {
            abort(404);
        }
        $comments = TaskComment::where('task_id', $id)->get();
        
        $solution = Solution::where('task_id', $id)->first();
        
        return view('task.showTask', compact('task', 'comments', 'solution'));

    }

    public function storeTaskComment(Request $request, $id) {

        $this->validate(request(), [
            'comment' => 'required'
        ], [
            'comment.required' => 'Введите комментарий'
        ]);
        $comment = new TaskComment();
        $comment->task_id = $id;
        $comment->student_id = Auth::user()->id;
        $comment->text = $request->comment;
        $comment->save();
        return redirect()->back();
    }

    
}

<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;

class SessionController extends Controller
{
    public function __construct() {

        $this->middleware('guest', [
            'except' => [
            'destroy'
        ]]);
        
    }

    public function create() {

        return view('session.createLogIn');

    }

    public function destroy() {

        Auth::logout();

        return redirect()->home();


    }

    public function store(Request $request) {

        $credentials = $request->only('email', 'password');
        $authenticated = Auth::attempt($credentials);
        // $user = User::where('id', Auth::user()->id)->first()->roles;
        // dd($user == 'admin');
        if($authenticated) {
            // dd($user->roles->name);
            
            if(Auth::user()->roles->first()->name == 'admin') {
                return redirect()->intended(route('showGroups'));
            }elseif(Auth::user()->roles->first()->name == 'teacher') {
                return redirect()->intended(route('teacherPanel'));
            }else {
                return redirect()->intended(route('studentPanel'));
            }

        }
        else {
        
            return back()->withInput()->withErrors([
               'message' => 'Вы ввели неправильные данные, повторите попытку.'
            ]);
        }

    }
    
    public function userRedirect() {
        if(Auth::check()) {
            if(Auth::user()->roles->first()->name == 'admin') {
                return redirect()->intended(route('showGroups'));
            }elseif(Auth::user()->roles->first()->name == 'teacher'){
                return redirect()->intended(route('teacherPanel'));
            }else {
                return redirect()->intended(route('studentPanel'));
            }
        }else {
            return view('home');
        }
    }
}

<?php

namespace App\Http\Controllers;
use App\Report;
use App\ReportAnswer;
use Illuminate\Support\Facades\Auth;
use App\Group;
use App\User;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('can:admin_properties'||'can:student_properties'||'can:teacher_properties');
    }

    public function showReports() {
        $reports = Report::where('user_id', Auth::user()->id)->paginate(1);
        if(Auth::user()->roles->first()->name == 'student') {
            return view('reports.showReports', compact('reports'));
        }elseif(Auth::user()->roles->first()->name == 'teacher') {
            $group = Group::where('id', Auth::user()->teacher_group_id)->first();
            return view('reports.showReports', compact('reports', 'group'));            
        }
    }

    public function createReport() {
        if(Auth::user()->roles->first()->name == 'student') {
            return view('reports.createStudentReport');
        }elseif(Auth::user()->roles->first()->name == 'teacher') {
            $group = Group::where('id', Auth::user()->teacher_group_id)->first();
            return view('reports.createTeacherReport', compact('group'));            
        }
    }

    public function storeReport(Request $request) {
        $this->validate(request(), [
            'text' => 'required',

            'title' => 'required'
        ],[
            'text.required' => 'Введите текст обращения',
            'title.required' => 'Введите тему обращения'
        ]);

        $report = new Report();
        $report->user_id = Auth::user()->id;
        $report->title = $request->title;
        $report->text = $request->text;
        $report->save();
        
        if(Auth::user()->roles->first()->name == 'student') {
            return redirect()->route('showStudentReports');            
        }elseif(Auth::user()->roles->first()->name == 'teacher') {
            return redirect()->route('showTeacherReports');
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Group;
use App\User;
use App\Task;



class GroupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('can:admin_properties', ['only' => ['create', 'store', 'delete', 'index',
        'show', 'edit', 'update']]);
    }

    public function create() {
        
        $teachers = User::where('teacher_group_id', null)->whereHas('roles', function(Builder $query){
            $query->where('name', 'like', 'teacher');
        })->get();
        
        $students = User::where('student_group_id', null)->whereHas('roles', function(Builder $query){
            $query->where('name', 'like', 'student');
        })->get();

        return view('group.create', compact('teachers', 'students'));
    }

    public function store(Request $request) {
        $this->validate(request(), [

            'group_name' => 'required',

            'choose_teacher' => 'required',

            'choose_students' => 'required'

        ], [
            'group_name.required' => 'Введите название группы',

            'choose_teacher.required' => 'Выберите учителя',

            'choose_students.required' => 'Выберите учеников'
        ]);

        $students = $request->choose_students;
        $teacher = $request->choose_teacher;
    
        $group = new Group();
        $group->name = $request->group_name;
        $group->save();
        if($students) {
            foreach($students as $student) {
                $user = User::where('id', $student)->first();
                $user->student_group_id = $group->id;
                $user->save();
            }            
            // $group->students_count = count($students);
        }
        $group->save();
       $userForTeacher = User::where('id', $teacher)->first();
       $userForTeacher->teacher_group_id = $group->id;
       $userForTeacher->save();
    
        return redirect()->route('showGroups');
    }

    public function delete($id) {
        $group = Group::find($id);
        $students = User::where('student_group_id', $id)->whereHas('roles', function(Builder $query){
            $query->where('name', 'like', 'student');
        })->get();
        foreach($students as $student) {
            $student->student_group_id = null;
            $student->save();
        }
        $teacher = User::where('teacher_group_id', $id)->first();
        $teacher->teacher_group_id = null;
        $teacher->save();
        $group->delete();

        return redirect()->back();
    }

    public function show($id) {
        $group = Group::where('id', $id)->first();
        $students = User::where('student_group_id', $id)->get();
        $tasks = Task::where('group_id', $id)->get();
    
        return view('group.show', compact('group', 'students', 'tasks'));
    }

    public function index() {

        $groups = Group::paginate(5);

        return view('group.index', compact('groups'));
    }

    public function edit($id) {
        $group = Group::where('id', $id)->first();
        $groupStudents = User::where('student_group_id', $id)->get();
        $freeStudents = User::where('student_group_id', null)->whereHas('roles', function(Builder $query){
            $query->where('name', 'like', 'student');
        })->get();
        $teacher = User::where('teacher_group_id', $id)->first();
        $freeTeachers = User::where('teacher_group_id', null)->whereHas('roles', function(Builder $query){
            $query->where('name', 'like', 'teacher');
        })->get();

        return view('group.edit', compact('group', 'groupStudents', 'freeStudents', 'teacher', 'freeTeachers'));
    }

    public function update(Request $request, $id) {
        $this->validate(request(), [
            'teacher' => 'required',

            'name' => 'required'
        ], [
            'teacher.required' => 'Выберите учителя', 
            
            'name.required' => 'Введите название'
        ]);
        $group = Group::where('id', $id)->first();
        $oldTeacher = User::where('teacher_group_id', $id)->first();
        $newTeacher = User::where('id', $request->teacher)->first();        
        if($oldTeacher->id == $newTeacher->id) {

        }else {
            $newTeacher->teacher_group_id = $id;
            $oldTeacher->teacher_group_id = null;
            $oldTeacher->save();
            $newTeacher->save();
        }
        $students = $request->addStudents;
        if($students) {
            foreach($students as $student) {
                $user = User::where('id', $student)->first();
                $user->student_group_id = $group->id;
                $user->save();
            }
        }
        $group->name = $request->name;
        $group->save();


        return redirect()->route('showGroups');
    }
}

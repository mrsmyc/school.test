<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Group;
use App\User;
use App\Role;
use App\Task;
use App\Solution;
use App\TaskComment;
use App\Report;
use App\ReportAnswer;
use App\CommentAnswer;
use App\Question;
use App\QuestionAnswer;
use Illuminate\Support\Facades\Mail;
use App\Mail\ReportAnswered;
use App\Mail\TaskCreated;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('can:admin_properties');
    }
    public function index() {

        return view('admin.index');
    }

    public function indexTeachers() {
        $teachers = User::whereHas('roles', function(Builder $query){
            $query->where('name', 'like', 'teacher');
        })->paginate(5);

        return view('teacher.index', compact('teachers'));
    }

    public function createTeacher() {

        return view('teacher.create');

    }

    public function storeTeacher() {
        
        $this->validate(request(), [

            'name' => 'required',

            'email' => 'required|email|unique:users',

            'password' => 'required|confirmed'

        ]);

        $hashed = Hash::make(request()->password);
        
        $user = new User;

        $user->name = request()->name;
        
        $user->email = request()->email;

        $user->password = $hashed;
            
        $user->save();
        
        $role = Role::where('name', 'teacher')->first();

        $user->assignRole($role->id);

        return redirect()->route('showTeachers');
    
    }

    public function changeTeacher($id) {
        $teacher = User::where('teacher_group_id',$id)->first();
        $freeTeachers = User::where('teacher_group_id', null)->whereHas('roles', function(Builder $query){
            $query->where('name', 'like', 'teacher');
        })->get();

        return view('group.changeTeacher', compact('teacher','freeTeachers'));
    }

    public function changeTeacherStore(Request $request) {
        $this->validate(request(), [

            'oldTeacher' => 'required',

            'newTeacher' => 'required'

        ], [
            'newTeacher.required' => 'Выберите нового учителя'
        ]);

        $oldTeacher = User::where('id', $request->oldTeacher)->first();
        $newTeacher = User::where('id', $request->newTeacher)->first();        
        $newTeacher->teacher_group_id = $oldTeacher->teacher_group_id;
        $oldTeacher->teacher_group_id = null;
        $oldTeacher->save();
        $newTeacher->save();

        return redirect()->route('showGroups');
    }    

    public function indexStudent() {

        $students = User::whereHas('roles', function(Builder $query){
            $query->where('name', 'like', 'student');
        })->paginate(5);

        return view('student.index', compact('students'));

    }

    public function createStudent() {

        return view('student.create');        
    }

    public function storeStudent() {

        $this->validate(request(), [

            'name' => 'required',

            'email' => 'required|email|unique:users',

            'password' => 'required|confirmed'

        ]);

        $hashed = Hash::make(request()->password);
        
        $user = new User;

        $user->name = request()->name;
        
        $user->email = request()->email;

        $user->password = $hashed;
            
        $user->save();
        
        $role = Role::where('name', 'student')->first();

        $user->assignRole($role->id);

        return redirect()->route('showStudents');

    }

    public function studentGroupEnroll($id) {

        $student = User::where('id', $id)->first();
        $groups = Group::all();

        return view('student.enroll', compact('student', 'groups'));
    }

    public function studentGroupStore(Request $request) {
        $this->validate(request(), [
            'group' => 'required'
        ], [
            'group.required' => 'Выберите группу'
        ]);
        $group = Group::find($request->group);    
        $student = User::where('id', $request->studentId)->first();
        $student->student_group_id = $request->group;
        $student->save();
        $group->save(); 

        return redirect()->route('showStudents');
    }

    public function showStudent($id) {
        $student = User::find($id);
        $solutions = Solution::where('student_id', $id)->paginate(1);
        $comments = TaskComment::where('student_id', $id)->get();

        return view('admin_panel.showStudent', compact('student', 'solutions', 'comments'));
    }

    public function showTask($id) {
        $task = Task::find($id);
        $comments = TaskComment::where('task_id', $id)->paginate(1);
        
        return view('admin_panel.showTask', compact('task', 'comments'));
    }

    public function editTask($id) {
        $task = Task::find($id);
        
        return view('admin_panel.editTask', compact('task'));
    }

    public function updateTask(Request $request, $id) {
        $this->validate(request(), [
            'title' => 'required',

            'task' => 'required',

            'date' => 'required'
        ], [
            'title.required' => 'Введите предмет задания',

            'task.required' => 'Введите текст задания',

            'date.required' => 'Выберите дату'
        ]);
        $task = Task::find($id);
        $task->title = $request->title;
        $task->task = $request->task;
        $task->date = $request->date;
        $task->save();

        return redirect()->route('showGroup', $task->group_id);
    }

    public function deleteComment($taskId, $commentId) {
        $comment = TaskComment::find($commentId);
        $comment->delete();

        return redirect()->back();
    }

    public function editComment($taskId, $commentId) {
        $comment = TaskComment::find($commentId);
        $commentAnswer = CommentAnswer::where('comment_id', $commentId)->first();
        $task = Task::find($taskId);

        return view('admin_panel.editComment', compact('comment', 'commentAnswer', 'task'));
    }

    public function updateComment(Request $request, $taskId, $commentId) {
        $this->validate(request(), [
            'comment' => 'required',

            'answer' => 'required'
        ], [
            'comment.required' => 'Введите комментарий',

            'answer.required' => 'Введите ответ'
        ]);
        $comment = TaskComment::find($commentId);
        $comment->text = $request->comment;
        $comment->save();
        $commentAnswer = CommentAnswer::where('comment_id', $commentId)->first();
        if($commentAnswer){
            $commentAnswer->answer = $request->answer;
            $commentAnswer->save();
        }else {
            $teacher = User::where('teacher_group_id', $comment->student->student_group_id)->first();
            $answer = new CommentAnswer();
            $answer->comment_id = $commentId;
            $answer->teacher_id = $teacher->id;
            $answer->answer = $request->answer;
            $answer->save();
        } 
        
        return redirect()->route('showTaskAdmin', $taskId);
    }

    public function deleteSolution($studentId, $solutionId) {
        $solution = Solution::find($solutionId);
        $solution->delete();

        return redirect()->back();
    }

    public function editSolution($studentId, $solutionId) {
        $solution = Solution::find($solutionId);
        $student = User::find($studentId);

        return view('admin_panel.editSolution', compact('solution', 'student'));
    }

    public function updateSolution(Request $request, $studentId, $solutionId) {
        $this->validate(request(), [
            'text' => 'required',

            'answer' => 'required',

            'rating' => 'required'
        ], [
            'text.required' => 'Введите текст',

            'answer.required' => 'Введите ответ',

            'rating.required' => 'Введите оценку'
        ]);

        $solution = Solution::find($solutionId);
        $solution->text = $request->text;
        $solution->teacher_answer = $request->answer;
        $solution->rating = $request->rating;
        $solution->save();

        $student = User::find($solution->student_id);
        $solutions = Solution::where('student_id', $student->id)->get();
        $marksCount = 0;
        $marksSumm = 0;
        foreach($solutions as $solution) {
            if($solution->teacher_answer != null && $solution->rating !=null) {
                $marksCount++;
                $marksSumm = $marksSumm + $solution->rating;
            }
        }

        if($marksCount != 0 && $marksSumm != 0) {
            $student->student_rating = $marksSumm / $marksCount;
            $student->save();
        }

        return redirect()->route('studentSolution', $studentId);
    }

    public function deleteQuestion($studentId, $questionId) {
        $question = Question::find($questionId);
        $question->delete();

        return redirect()->back();
    }

    public function showQuestions($id) {
        $questions = Question::where('student_id', $id)->paginate(1);
        
        return view('admin_panel.showQuestions', compact('questions'));
    }

    public function createReportAnswer($id) {
        $report = Report::find($id);

        return view('admin_panel.createReportAnswer', compact('report'));
    }

    public function storeReportAnswer(Request $request, $id) {
        $this->validate(request(), [
            'text' => 'required',

            'title' => 'required'
        ], [
            'text.required' => 'Введите текст ответа',

            'title.required' => 'Введите тему ответа'
        ]);
        $answer = ReportAnswer::where('report_id', $id)->first();
        if($answer == null) {
            $newAnswer = new ReportAnswer();
            $newAnswer->title = $request->title;
            $newAnswer->text = $request->text;
            $newAnswer->report_id = $id;
            $newAnswer->save();
        }else {
            $answer->title = $request->title;
            $answer->text = $request->text;
            $answer->save();
        }
        $report = Report::find($id);
        $user = User::find($report->user_id);
        $admin = Auth::user();
        Mail::to($user)->send(new ReportAnswered($admin,$answer, $user));

        return redirect()->route('showAdminReports');
    }

    public function editReportAnswer($reportId, $answerId) {
        $answer = ReportAnswer::find($answerId);

        return view('admin_panel.editReportAnswer', compact('answer'));
    }

    public function updateReportAnswer(Request $request, $reportId, $answerId) {
        $this->validate(request(), [
            'text' => 'required',

            'title' => 'required'
        ], [
            'text.required' => 'Введите текст',
            
            'title.required' => 'Введите тему'
        ]);
        $answer = ReportAnswer::find($answerId);
        $answer->title = $request->title;
        $answer->text = $request->text;
        $answer->save();

        return redirect()->route('showAdminReports');
    }

    public function editQuestion($studentId, $questionId) {
        $question = Question::find($questionId);
        $answer = QuestionAnswer::where('question_id', $questionId)->first();        

        return view('admin_panel.editQuestion', compact('question', 'answer'));
    }

    public function updateQuestion(Request $request, $studentId, $questionId) {
        $this->validate(request(), [
            'text' => 'required',

            'answer' => 'required'
        ], [
            'text.required' => 'Введите текст',

            'answer.required' => 'Введите ответ'
        ]);
        $question = Question::find($questionId);
        $question->text = $request->text;
        $question->save();
        $answer = QuestionAnswer::where('question_id', $questionId)->first();
        if($answer != null) {
            $answer->text = $request->answer;
            $answer->save();
        }else {
            $newAnswer = new QuestionAnswer();
            $newAnswer->text = $request->answer;
            $newAnswer->teacher_id = Auth::user()->id;
            $newAnswer->question_id = $question->id;
            $newAnswer->save();
        }
        

        return redirect()->route('showAdminQuestions', $studentId);
    }


    public function reports() {
        $reports = Report::paginate(1);

        return view('admin_panel.reports', compact('reports'));
    }

    public function deleteReport($id) {
        $report = Report::find($id);
        $report->delete();

        return redirect()->back();
    }

    public function watchTaskSolution($id) {
        $solutions = Solution::where('task_id', $id)->paginate(1);

        return view('admin_panel.showTaskSolution', compact('solutions'));
    }

    public function editTaskSolution($taskId, $solutionId) {
        $solution = Solution::find($solutionId);

        return view('admin_panel.editTaskSolution', compact('solution'));
    }

    public function updateTaskSolution(Request $request, $taskId, $solutionId) {
        $this->validate(request(), [
            'text' => 'required',

            'answer' => 'required',

            'rating' => 'required',
        ], [
            'text.required' => 'Введите текст',

            'answer.required' => 'Введите ответ',

            'rating.required' => 'Дайте оценку'
        ]);
        $solution = Solution::find($solutionId);
        $solution->text = $request->text;
        $solution->teacher_answer = $request->answer;
        $solution->rating = $request->rating;
        $solution->save();
        $student = User::find($solution->student_id);

        $solutions = Solution::where('student_id', $student->id)->get();
        $marksCount = 0;
        $marksSumm = 0;
        foreach($solutions as $solution) {
            if($solution->teacher_answer != null && $solution->rating !=null) {
                $marksCount++;
                $marksSumm = $marksSumm + $solution->rating;
            }
        }

        if($marksCount != 0 && $marksSumm != 0) {
            $student->student_rating = $marksSumm / $marksCount;
            $student->save();
        }

        return redirect()->route('watchTaskSolution', $taskId);
    }

    

    public function createTask($id) {
                
        $group = Group::where('id', $id)->first();

        return view('task.create', compact('group'));

    }

    public function storeTask(Request $request) {
        
        $students = User::where('student_group_id', $request->groupId)->get();
        $teacher = User::where('teacher_group_id', $request->groupId)->first();
        $this->validate(request(), [

            'task' => 'required',

            'title' => 'required',

            'date' => 'required|date'

        ], [
            'task.required' => 'Введите задание',

            'title.required' => 'Введите предмет задания',
            
            'date.required' => 'Выберите дату'
        ]);
        $task = new Task();
        $task->group_id = $request->groupId;
        $task->task = $request->task;
        $task->title = $request->title;
        $task->deadline = $request->date;
        $task->save();

        $author = Auth::user();
        $usersEmails = User::where('student_group_id', $request->groupId)->get();
        foreach($usersEmails as $user) {
            Mail::to($user)->send(new TaskCreated($task, $author));
        }
        
        if(Auth::user()->roles->first()->name == 'teacher') {
            return redirect()->route('teacherTasks',$request->groupId);
        }else {
            return redirect()->route('showGroup',$request->groupId);
        }

    }

    public function deleteTask($id) {
        $task = Task::find($id);
        $task->delete();

        return redirect()->back();
    }
    
}

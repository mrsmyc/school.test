<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    
    public function firstTask() {
        return view('testTask.firstTask');
    }

    public function secondTask() {
        return view('testTask.secondTask');
    }

    public function thirdTask() {
        return view('testTask.thirdTask');
    }

    public function firstTaskStore(Request $request) {
        $letterGet = $request->letter;
        return dd($this->letterToNum($letterGet));
    }

    public function secondTaskStore(Request $request) {
        $number = $request->number;
        return dd($this->numToLetter($number));
        // return dd($this->getColumnFromNum($number));
    }

    private function getColumnFromNum($num)
    {
     $numeric = ($num - 1) % 26;
     $letter = chr(65 + $numeric);
     $num2 = intval(($num - 1) / 26);
     if ($num2 > 0) {
         return $this->getColumnFromNum($num2) . $letter;
     } else {
         return $letter;
     }
    }

    public function letterToNum($letterGet) {
        $alphabet = range('A', 'Z');
        $letterGet = mb_strtoupper($letterGet);
        $letters = preg_split('//', $letterGet, -1, PREG_SPLIT_NO_EMPTY);
        $output = 0;
        $lettersAmount = count($letters)-1;
        foreach($letters as $letter) {
            $sum = (array_search($letter, $alphabet)+1) * (pow(26, $lettersAmount));
            $output += $sum;
            $lettersAmount--;
        }
        return $output;
    }

    public function numToLetter($number) {
        // Работает, исключая компбинации zz
        
        $alphabet = range('A', 'Z');
        $alphabetZero = [
            '26' => 'Z',
            '1' => 'A', 
            '2' => 'B', 
            '3' => 'C', 
            '4' => 'D', 
            '5' => 'E', 
            '6' => 'F', 
            '7' => 'G', 
            '8' => 'H', 
            '9' => 'I', 
            'a' => 'J', 
            'b' => 'K', 
            'c' => 'L', 
            'd' => 'M', 
            'e' => 'N', 
            'f' => 'O', 
            'g' => 'P', 
            'h' => 'Q', 
            'i' => 'R', 
            'j' => 'S', 
            'k' => 'T', 
            'l' => 'U', 
            'm' => 'V', 
            'n' => 'W', 
            'o' => 'X', 
            'p' => 'Y', 
            '0' => 'Z', 
        ];
        $output = '';
        $newSys = [];
        $count = 0;
        $mod = $number % 26;
        $newSys[$count] = $mod;
        if($number % 26 == 0){
                // $mod = $number / 26;
                $newSysNum = base_convert($number-26, 10, 26);
                $nums = preg_split('//', $newSysNum, -1, PREG_SPLIT_NO_EMPTY);        
                foreach ($nums as $num) {
                    $output .= $alphabetZero[$num];
                }
        }else {
            while($number > 0) {
                        $int = intdiv($number, 26);
                        if($int % 26 >= 1){
                            $count++;        
                            $newSys[$count] = $int % 26;
                        }
                        $number = $int;
                    }
                    $newSys = array_reverse($newSys);
                    foreach($newSys as $num){
                        $output .= $alphabet[$num-1];
                    }
        }
        return $output;
        
        
        
        //////Работает но очень криво
        // $alphabet = range('A', 'Z');
        // $output = '';
        // $newSys = [];
        // $count = 0;
        // $mod = $number % 26;
        // $newSys[$count] = $mod;
        // if($number % 26 == 0){
        //         $mod = $number / 26;
        //         if($mod == 1) {
        //             $output = 'Z'; 
        //         }else {
        //             $count = 2;
        //             foreach($alphabet as $letter) {
        //                 if($count == $mod) {                        
        //                     $output = $letter . 'Z'; 
        //                 }
        //                 $count++;
        //             }
        //         }
        // }else {
        //     while($number >= 1) {
        //                 $int = intdiv($number, 26);
        //                 if($int % 26 >= 1){
        //                     $count++;        
        //                     $newSys[$count] = $int % 26;
        //                 }
        //                 $number = $int;
        //             }
        //             $newSys = array_reverse($newSys);
        //             foreach($newSys as $num){
        //                 $output .= $alphabet[$num-1];
        //             }
        // }
        // return $output;
    }

    public function letterShift(Request $request) {
        $letterGet = $request->letter;
        $numberGet = $request->number;
        $letterToNumber = $this->letterToNum($letterGet);
        $letterToNumber += $numberGet;
        $output = $this->numToLetter($letterToNumber);
        dd($output);

    }

////////////округление цены

public function fourthTask(){
    return view('testTask.fourthTask');
}

public function priceRound(Request $request) {
    $number = $request->number;
    $loss = $request->loss;
    $percent = 7;
    $number = round($number * (1 + $percent / 100));
    $mod = $number % 1000;
    $int = $number - $mod;

    if($mod >= 499 && $mod <= 499 + $loss) {
        $mod = 499;
        $output = $int + $mod;
    } elseif ($mod - $loss <= 0) {
        $mod = 999;
        $output = $int-1000 + $mod;
    } elseif (!$mod - $loss <= 0 && $mod < 499) {
        $mod = 499;
        $output = $int + $mod;
    } else {
        $mod = 999;
        $output = $int + $mod;
    }
    dd($number, $output);
}




}


/////решение 2 задания
  // $array = range('A', 'Z');
        // $number = $request->number;
        // $output = '';
        // $newArray = [];
        // $newCount = 0;
        // $middle = $number % 26;
        // $newArray[$newCount] = $middle;
        // if($number % 26 == 0){
        //     $middle = $number / 26;
        //     if($middle == 1) {
        //         $output = 'Z'; 
        //     }else {
        //         $count = 2;
        //         foreach($array as $arr) {
        //             if($count == $middle) {                        
        //                 $output = $arr . 'Z'; 
        //             }
        //             $count++;
        //         }
        //     }
        // }else{
        //     while($number >= 1) {
        //         $int = intdiv($number, 26);
        //         if($int % 26 >= 1){
        //             $newCount++;

        //             $newArray[$newCount] = $int % 26;
        //         }
        //         $number = $int;
        //     }
        //     $newArray = array_reverse($newArray);
        //     foreach($newArray as $newArr){
        //         $count = 1;
        //         foreach($array as $arr) {
        //             if($newArr == $count){
        //                 $output .= $arr;
        //             }
        //             $count++;
        //         }
        //     }
        // }        
        // dd($output);
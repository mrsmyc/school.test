<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Report extends Model
{
    use SoftDeletes;

    public function author() {
        return $this->belongsTo(User::class, 'user_id');
    }


    public function answer() {
        return $this->hasMany(ReportAnswer::class, 'report_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Solution extends Model
{
    use SoftDeletes;

    public function student() {
        return $this->belongsTo(User::class, 'student_id');
    }

    public function task() {
        return $this->belongsTo(Task::class, 'task_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReportAnswer extends Model
{
    use SoftDeletes;

    public function report() {
        return $this->belongsTo(Report::class, 'report_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Group extends Model
{
    use SoftDeletes;

    public function teacher() {
        return $this->hasOne(User::class, 'teacher_group_id');
    }

    public function students() {
        return $this->hasMany(User::class, 'student_group_id');
    }
}

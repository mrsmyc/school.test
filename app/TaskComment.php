<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaskComment extends Model
{
    use SoftDeletes;

    public function student() {
        return $this->belongsTo(User::class, 'student_id');
    }

    public function task() {
        return $this->belongsTo(Task::class, 'task_id');
    }

    public function answer() {
        return $this->hasMany(CommentAnswer::class, 'comment_id');
    }
}

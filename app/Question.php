<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends Model
{
    use SoftDeletes;

    public function task() {
        return $this->belongsTo(Task::class, 'task_id');
    }

    public function student() {
        return $this->belongsTo(User::class, 'student_id');
    }

    public function answer() {
        return $this->hasMany(QuestionAnswer::class, 'question_id');
    }
}

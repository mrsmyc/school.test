<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CommentAnswer extends Model
{
    use SoftDeletes;
    
    public function author() {
        return $this->belongsTo(User::class, 'teacher_id');
    }
}

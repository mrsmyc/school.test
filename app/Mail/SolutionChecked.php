<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Task;
use App\Solution;
use App\User;
use Illuminate\Database\Eloquent\Collection;

class SolutionChecked extends Mailable
{
    use Queueable, SerializesModels;


    public $teacher;
    public $solution;
    public $task;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $teacher, Solution $solution, Task $task)
    {
        $this->teacher = $teacher;
        $this->task = $task;
        $this->solution = $solution;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.SolutionChecked');
    }
}

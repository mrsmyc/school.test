<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Question;
use App\User;

class QuestionStored extends Mailable
{
    use Queueable, SerializesModels;

    public $question;
    public $student;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Question $question, User $student)
    {
        $this->question = $question;
        $this->student = $student;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.questionStored');
    }
}

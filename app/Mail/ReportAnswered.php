<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\User;
use App\ReportAnswer;



class ReportAnswered extends Mailable
{
    use Queueable, SerializesModels;

    public $answer;
    public $admin;
    public $user;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $admin, ReportAnswer $answer, User $user)
    {
        $this->admin = $admin;
        $this->answer = $answer;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.reportAnswered');
    }
}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Task;
use App\User;

class TaskCompleted extends Mailable
{
    use Queueable, SerializesModels;

    public $task;
    public $student;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $student, Task $task)
    {
        $this->student = $student;
        $this->task = $task;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.taskCompleted');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuestionAnswer extends Model
{
    use SoftDeletes;

    public function teacher() {
        return $this->belongsTo(User::class, 'teacher_id');
    }    
    
}

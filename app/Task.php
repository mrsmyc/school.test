<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use SoftDeletes;

    public function group() {
        return $this->belongsTo(Group::class, 'group_id');
    }

    public function solutions() {
        return $this->hasMany(Solution::class, 'task_id');
    }

   

}

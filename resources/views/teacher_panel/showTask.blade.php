@extends('layouts.app')
@section('title')
    Задание {{$task->title}}
@endsection
@section('content')
{{-- @include('inc.navi') --}}
    <div class="mt-3">
        <h3>Заголовок задания: {{$task->title}}</h3>
    </div>
    <div>
        <h3>Задание: {{$task->task}}</h3>
    </div>
    <div>
        @if ($comments->isEmpty())
            <h5 class="mt-2">Вопросов еще нет</h5>
        @else
            <h5 class="mt-2">Вопросы: </h5>
            @foreach ($comments as $comment)
                <div class="card mt-3">
                    <div class="card-body">
                        <p>{{$comment->text}}</p>
                        <span class="border-top">Автор: {{$comment->student->name}}</span>
                    </div>
                    
                    <div class="card-footer">
                        @forelse ($comment->answer as $answer)
                            <p>Ответ учителя: {{$answer->answer}}</p>                                                                                   
                        @empty
                            <form action="/teacher/task/{{$comment->id}}" method="POST">
                                @csrf
                                <input value="{{$task->id}}" name="taskId" type="text" hidden>
                                <div class="form-group">
                                    <label for="answer">Ответ:</label>
                                    <input class="form-control" name="answer" id="answer" type="text">
                                </div>
                                <button class="btn btn-primary mt-2">Отправить</button>
                            </form>
                        @endforelse                     
                    </div>
                </div>
            @endforeach
            <div class="mt-3 d-flex justify-content-center">
                {{ $comments->appends(request()->toArray())->links() }}                
            </div>
        @endif
    </div>
@endsection
@extends('layouts.app')
@section('title')
    Задания
    @endsection
@section('content')
    {{-- @include('inc.navi') --}}
    <div class="mt-3">        
        <h3>Задания для класса: </h3>
        <div class=" mt-3">
            <a href="/teacher/group/{{$group->id}}/tasks/create" class="btn btn-primary w-25">Добавить задание</a>
        </div>
        @foreach ($tasks as $task)
            <div class="card mt-3">
                <div class="card-header">
                    <p>Заголовок: {{$task->title}}</p>
                </div>
                <div class="card-body">
                    <p>Задание: {{$task->task}}</p>
                    <a href="/teacher/task/{{$task->id}}" class="btn btn-primary">Вопросы учеников</a>
                    <a href="/teacher/task/{{$task->id}}/answers" class="btn btn-primary">Ответы учеников</a>
                    <a href="/teacher/group/{{$task->group->id}}/tasks/{{$task->id}}/redact" class="btn btn-primary"><i class="fas fa-edit"></i></a>
                </div>
                <div class="card-footer">
                    {{-- <p>Крайний срок: {{$task->deadline}}</p> --}}
                    <p>Крайний срок: {{\Carbon\Carbon::parse($task->deadline)->format('d.m.Y')}}</p>
                    <p class="text-warning">Выполнило: {{$task->solutions->count()}} учеников из {{$students->count()}}</p>
                    <p class="text-success">Проверено: @if ($task->completed == null) 0 @else {{$task->completed}} @endif из {{$task->solutions->count()}}</p>
                </div>
            </div>
        @endforeach
        <div class="mt-3 d-flex justify-content-center">
            {{ $tasks->appends(request()->toArray())->links() }}                
        </div>
    </div>
@endsection
@extends('layouts.app')
@section('title')
    Успеваемость {{$student->name}}
@endsection
@section('content')
    {{-- @include('inc.navi',['group' => $group]) --}}
<div class="mt-3">
    <h3>Все задания и оценки ученика - {{$student->name}}</h3>
    <div>
        @foreach ($solutions as $solution)
            <div class="card mt-3">
                <div class="card-header">
                    <p>Заголовок: {{$solution->task->title}}</p>
                </div>
                <div class="card-body">
                    <p>Задание: {{$solution->task->task}}</p>
                    @if ($solution->rating != null && $solution->teacher_answer != null)
                        <p class="text-success">Оценка: {{$solution->rating}}</p>
                        <p class="text-success">Ваш комментарий: {{$solution->teacher_answer}}</p>
                    @endif
                </div>
                <div class="card-footer">                    
                    @if (\Carbon\Carbon::parse($solution->created_at)->format('d.m.Y') < \Carbon\Carbon::parse($solution->task->deadline)->format('d.m.Y'))
                        <p class="text-success">Дата получения: {{\Carbon\Carbon::parse($solution->created_at)->format('d.m.Y')}}</p>
                        <p class="">Крайний срок: {{\Carbon\Carbon::parse($solution->task->deadline)->format('d.m.Y')}}</p>
                    @else
                        <p class="text-danger">Дата получения: {{\Carbon\Carbon::parse($solution->created_at)->format('d.m.Y')}}</p>
                        <p class="">Крайний срок: {{\Carbon\Carbon::parse($solution->task->deadline)->format('d.m.Y')}}</p>
                    @endif
                </div>
            </div>
        @endforeach
        <div class="mt-3 d-flex justify-content-center">
            {{ $solutions->appends(request()->toArray())->links() }}                
        </div>
    </div>
</div>
@endsection
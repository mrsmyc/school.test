@extends('layouts.app')
@section('title')
    Ответы на задание {{$task->title}}
@endsection
@section('content')
    {{-- @include('inc.navi') --}}
    <div class="mt-3">
        <h3>Ответы от учеников на задание - {{$task->title}}</h3>
        <h3>{{$task->task}}</h3>
    </div>
    <div>
        @if ($solutions->isEmpty())
            <h5>Ответов еще нет</h5>
        @else
        @foreach ($solutions as $solution)
        <div class="card mt-3">
            <div class="card-header">
                <span>Ученик: {{$solution->student->name}}</span>
            </div>
            <div class="card-body">
                <p>Ответ: {{$solution->text}}</p>
            </div>
            <div class="card-footer">
                @if ($solution->rating==null && $solution->teacher_answer==null)
                <form method="POST" action="/teacher/task/{{$solution->id}}/answers/rating">
                    @csrf
                    <input type="text" value="{{$task->id}}" hidden name="groupId">
                    <div class="form-group">
                        <label for="rating">Оценить работу</label>
                        <input name="rating" id="rating" class="mt-2 form-control w-25" min="1" max="5" type="number">
                    </div>

                    <div class="form-group">
                        <label for="comment">Оставить комментарий к работе</label>
                        <textarea class="form-control" name="comment" id="comment" cols="30" rows="2"></textarea>
                    </div>
                    <button class="mt-3 btn btn-primary">Оценить</button>
                </form>
                @else
                <p>Оценка: {{$solution->rating}}</p>
                    <p>Ваш комментарий: {{$solution->teacher_answer}}</p>                
                @endif
            </div>
        </div>
    @endforeach
    <div class="mt-3 d-flex justify-content-center">
        {{ $solutions->appends(request()->toArray())->links() }}                
    </div>
        @endif

    </div>
@endsection
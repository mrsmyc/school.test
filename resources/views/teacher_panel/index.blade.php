@extends('layouts.app')
@section('title')
    Личный кабинет учителя
@endsection
@section('content')
    {{-- @include('inc.navi',['group' => $group]) --}}
    {{-- <h3>Ваш класс - {{$group->name}}</h3> --}}
    <div class="mt-3">
        <h3>Ваши ученики: </h3>
        @foreach ($students as $student)
            <div class="card mt-3">
                <div class="card-body">
                    <a href="/teacher/group/student/{{$student->id}}/marks">{{$student->name}}</a>
                    <p>Выполнено заданий: {{$student->solutions->count()}} из {{$tasks->count()}}</p>
                </div>
                {{-- <div class="card-footer">
                    <div>
                        <a href="/teacher/group/student/{{$student->id}}/marks">Подробнее</a>
                    </div>
                </div> --}}
            </div>
        @endforeach
        <div class="mt-3 d-flex justify-content-center">
            {{ $students->appends(request()->toArray())->links() }}                
        </div>
    </div>
@endsection
@extends('layouts.app')
@section('title')
    Вопросы от учеников
@endsection
@section('content')
    {{-- @include('inc.navi',['group' => $group]) --}}
    <div class="mt-3">
        @if ($questions == null)
            <h3>Вопросов нет</h3>
        @else
            <h3>Вопросы учеников:</h3>
            @foreach ($questions as $question)
                <div class="card mt-3">
                    <div class="card-header">
                        <p>Вопрос от: {{$question->student->name}}</p>
                        <p>На задание: {{$question->task->title}} | {{$question->task->task}}</p>
                    </div>
                    <div class="card-body">
                        <p>{{$question->text}}</p>
                    </div>
                    <div class="card-footer">
                        @forelse ($question->answer as $answer)
                            <p>Ваш ответ: {{$answer->text}}</p>
                        @empty
                            <form  action="/teacher/questions/{{$question->id}}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label for="text">Ответить:</label>
                                    <textarea class="form-control" name="text" id="text" cols="30" rows="2"></textarea>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="w-25 mt-3 btn btn-primary form-control">Ответить</button>
                                </div>
                            </form>
                        @endforelse
                        @include('inc.errors')
                    </div>
                </div>
            @endforeach
            <div class="mt-3 d-flex justify-content-center">
                {{ $questions->appends(request()->toArray())->links() }}                
            </div>
        @endif
    </div>
@endsection
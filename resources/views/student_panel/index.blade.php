@extends('layouts.app')
@section('title')
    Личный кабинет
@endsection
@section('content')
    <div class="mt-3">
        <h3>Мои задания</h3>
        @if ($tasks->isEmpty())
            <h3>Заданий еще нет</h3>
        @else
            @foreach ($tasks as $task)
                <div class="card mt-3">
                    <div class="card-header">
                        <a href="/student/task/{{$task->id}}" class="text-primary ">{{$task->title}}</a>
                    </div>
                    <div class="card-body">
                        <p>{{$task->task}}</p>
                        <div class="border-top">
                         
                        <div class="mt-2">
                            @foreach ($solutions as $solution)
                                @if ($solution->task_id == $task->id)
                                    @if ($solution->rating != null && $solution->teacher_answer != null)
                                        <p class="text-success">Оценка: {{$solution->rating}}</p>
                                        <p class="text-success">Комментарий учителя: {{$solution->teacher_answer}}</p>
                                    @else
                                        <p class="text-warning">Ваша работа на проверке у учителя</p>
                                    @endif                                    
                                @else
                                    {{-- <a href="/student/task/{{$task->id}}/solution/add" class="btn btn-primary">Добавить ответ на задание</a> --}}
                                @endif                                
                            @endforeach
                            {{-- <a class="ml-3 btn btn-primary" href="/student/task/{{$task->id}}">Подробнее</a> --}}
                        </div>
                    </div>
                    </div>
                    <div class="card-footer">
                        <span>Крайний срок: {{\Carbon\Carbon::parse($task->deadline)->format('d.m.Y')}}</span>
                        <a class="float-right btn btn-primary" href="/student/task/{{$task->id}}/question">Вопрос учителю</a>
                    </div>
                </div>
            @endforeach
            <div class="mt-3 d-flex justify-content-center">
                {{ $tasks->appends(request()->toArray())->links() }}                
            </div>
        @endif
    </div>
@endsection
@extends('layouts.app')
@section('title')
    Мои оценки
@endsection
@section('content')
    <div class="mt-3">
        <div>
            <h3>Средний бал: {{Auth::user()->student_rating}}</h3>
        </div>
        <h3 class="mt-3 border-top">Мои оценки</h3>
        @if ($solutions->isEmpty())
            <h3>Оценок еще нет</h3>
        @else
            @foreach ($solutions as $solution)
                <div class="card mt-3">
                    <div class="card-header">
                        <h5>Ответ на задание: {{$solution->task->title}}</h5>
                    </div>
                    <div class="card-body">
                        <p>Ответ учителя: {{$solution->teacher_answer}}</p>
                    </div>
                    <div class="card-footer">
                        <p class="text-success">Оценка: {{$solution->rating}}</p>
                    </div>
                </div>
            @endforeach
            <div class="mt-3 d-flex justify-content-center">
                {{ $solutions->appends(request()->toArray())->links() }}                
            </div>
        @endif
    </div>
@endsection
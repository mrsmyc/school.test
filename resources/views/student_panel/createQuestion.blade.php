@extends('layouts.app')
@section('title')
    Задать вопрос
@endsection
@section('content')
    @include('inc.navi')
    <div class="mt-3">
        <h3>Задать вопрос учителю по заданию: {{$task->title}}</h3>
        <p class="mt-3 border-top border-bottom">{{$task->task}}</p>
        <div>
            <form action="/student/task/{id}/question" method="POST">
                @csrf
                <input name="taskId" value="{{$task->id}}" type="text" hidden>
                <div class="form-group">
                    <label for="text">Вопрос:</label>
                    <textarea class="form-control" name="text" id="text" cols="30" rows="2"></textarea>
                </div>
                <div class="mt-3 form-group">
                    <button class="btn btn-primary" type="submit">Отправить</button>
                </div>
            </form>
            @include('inc.errors')
        </div>
        
    </div>
@endsection
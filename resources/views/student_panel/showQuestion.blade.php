@extends('layouts.app')
@section('title')
    Вопрос на задание: {{$task->title}}
@endsection
@section('content')
    @include('inc.navi')
    <div class="mt-3">
        <h3>Заголовок задания: {{$task->title}}</h3>
        <h3 class="mt-3">Задание: {{$task->task}}</h3>
        <div class="mt-3">
            <h4>Ваш вопрос:</h4>
            <p class="border-top">{{$question->text}}</p>
        </div>
        <div class="border-top mt-3">
            @if ($answer == null)
                <h5>Учитель еще не ответил на ваш вопрос</h5>
            @else
                <h4>Ответ учителя:</h4>
                <p>{{$answer->text}}</p>
            @endif
        </div>
        
    </div>
@endsection
@extends('layouts.app')
@section('title')
    Редактирование решения 
@endsection
@section('content')
{{-- @include('inc.navi') --}}
   <div class="mt-3 w-50 mx-auto">
       <h3>Редактирование ответа на задание: </h3>
       <h5 class="mt-2">Заголовок задания: {{$solution->task->title}}</h5>
       <h5 class="border-top mt-2">Задание: {{$solution->task->task}}<h5>
    <form method="POST" action="/student/task/{{$solution->student->id}}/solution/{{$solution->id}}/edit">        
        @csrf
        <div class="form-group">
            <label for="text">Ваше решение:</label>
            <textarea class="form-control" name="text" id="text" cols="30" rows="4" required>{{$solution->text}}</textarea>            
        </div>

        <div class="form-group mt-3">
            <button class="btn btn-primary form-control" type="submit">Сохранить</button>
        </div>
        @include('inc.errors')
    </form>
   </div>
@endsection
@extends('layouts.app')
@section('title')
    Ответ на задание
@endsection
@section('content')
    <div>
        <h1>Добавьте ответ на задание:</h1>
        <h3> {{$task->title}}</h3>
        <p>{{$task->task}}</p>
        <div class="border-top">
            <form action="" method="POST">
                @csrf
                <input name="taskId" type="text" value="{{$task->id}}" hidden>
                <div class="form-group mt-3">
                    <label for="text">Ответ:</label>
                    <textarea class="form-control" name="text" id="" cols="30" rows="5"></textarea>
                </div>
                <div class="form-group mt-2">
                    <button class="w-25 btn btn-primary form-control" type="submit">Отправить</button>
                </div>
            </form>
        </div>
    </div>
@endsection
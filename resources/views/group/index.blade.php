@extends('layouts.app')
@section('title')
    Все классы
@endsection
@section('content')
    @include('inc.navi')
    <div class="row mt-3">
        <a href="/admin/groups/create" class="btn btn-primary w-25">Добавить класс</a>
    </div>
    <h3 class="mt-3">Всего классов: {{$groups->total()}}</h3>
    <div class="mt-3">
        @if ($groups->isNotEmpty())
            @foreach ($groups as $group)
                <div class="card mt-3">
                    <div class="card-header">
                        <a href="/admin/groups/show/{{ $group->id }}">{{ $group->name }}</a>
                    </div>
                    <div class="card-body">
                        <h4>Учитель: {{ $group->teacher->name }}</h4>
                        {{-- <h4>Учеников: {{ $group->students_count }}</h4> --}}
                        <h4>Учеников: {{ $group->students->count() }}</h4>
                        {{-- <a class="btn btn-primary" href="/admin/groups/show/{{ $group->id }}">Подробнее</a> --}}
                        <a class="btn btn-primary" href="/admin/groups/{{$group->id}}/redact"><i class="fas fa-edit"></i></a>
                        <a class="btn btn-primary" href="/admin/groups/{{$group->id}}/delete"><i class="fas fa-trash-alt"></i></a>
                        {{-- <a href="/admin/groups/{{$group->id}}/teacher/change" class="ml-3">Сменить учителя</a> --}}
                    </div>
                </div>
            @endforeach
        @else
            <h3>Классов еще нет</h3>
        @endif
    </div>
    <div class="mt-3">
        {{ $groups->appends(request()->toArray())->links() }}
    </div>
@endsection
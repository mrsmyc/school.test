@extends('layouts.app')
@section('title')
    Сменить учителя
@endsection
@section('content')
    <div class="mt-3">
        <h5>Учитель в данный момент: {{$teacher->name}}</h5>
    </div>
    <form action="/admin/groups/{id}/teacher/change" method="POST">
        @csrf
        <input type="text" name="oldTeacher" value="{{$teacher->id}}" hidden>
        <div class="form-group">
            @if ($freeTeachers->isEmpty())
                <label for="">Нет свободных учителей</label>
            @else
            <label for="newTeacher">Выберите нового учителя: </label>
            <select class="form-control" name="newTeacher" id="newTeacher" required>
                @foreach ($freeTeachers as $teacher)
                    <option value="{{$teacher->id}}">{{$teacher->name}}</option>
                @endforeach
            </select>
            @endif
        </div>
        <div class="form-group mt-3">
            <button class="btn btn-primary" type="submit">Сохранить</button>
        </div>
    </form>
    
@endsection
@extends('layouts.app')
@section('title')
    Редактирование класса
@endsection
@section('content')
{{-- @include('inc.navi') --}}
   <div class="mt-3 w-50 mx-auto">
       <h3>Редактирование класса "{{$group->name}}"</h3>
    <form method="POST" action="/admin/groups/{{$group->id}}/redact">
        @csrf
        <div class="form-group">
            <label for="name">Название класса:</label>
            <input name="name" id="name" class="form-control" type="text" value="{{$group->name}}" required>
        </div>

        <div class="form-group">
            <label for="teacher">Учитель:</label>
            
            <select class="form-control" name="teacher" id="teacher" required>
                @if ($teacher != null)
                    <option selected value="{{$teacher->id}}">{{$teacher->name}}</option>                    
                @endif
                @foreach ($freeTeachers as $freeTeacher)
                    <option value="{{$freeTeacher->id}}">{{$freeTeacher->name}}</option>
                @endforeach
            </select>
            
        </div>

        <div class="form-group">
            <label for="addStudents">Добавьте учеников в класс:</label>
            <select class="form-control" name="addStudents[]" id="addStudents" multiple >
                @foreach ($freeStudents as $freeStudent)
                    <option value="{{$freeStudent->id}}">{{$freeStudent->name}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group mt-3">
            <button class="btn btn-primary form-control" type="submit">Сохранить</button>
        </div>
        @include('inc.errors')
    </form>
   </div>
@endsection
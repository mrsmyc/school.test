@extends('layouts.app')
@section('title')
    Создание класса
@endsection
@section('content')
   <div class="mt-3 w-50 mx-auto">
    <form method="POST" action="/admin/groups/create">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="group_name">Введите название класса:</label>
            <input name="group_name" id="group_name" class="form-control" type="text" required>
        </div>

        <div class="form-group">
            <label for="choose_teacher">Добавьте учителя в класс:</label>
            @if ($teachers->isEmpty())
                <h3>Свободных учителей нет</h3>
            @else
            <select class="form-control" name="choose_teacher" id="choose_teacher" required>
                @foreach ($teachers as $teacher)
                    <option value="{{$teacher->id}}">{{$teacher->name}} @if ($teacher->groupAsTeacher != null)
                       - преподает в классе <<{{$teacher->groupAsTeacher->name}}>>
                    @endif</option>
                @endforeach
            </select>
            @endif
        </div>

        <div class="form-group">
            <label for="choose_students">Добавьте учеников в класс:</label>
            @if ($students->isEmpty())
                <h3>Свободных учеников нет</h3>
            @else
                <select class="form-control" name="choose_students[]" id="choose_students" multiple >
                    @foreach ($students as $student)
                        <option value="{{$student->id}}">{{$student->name}} @if ($student->groupAsStudent != null)
                        - является учеником класса <<{{$student->groupAsStudent->name}}>>  
                        @endif</option>
                    @endforeach
                </select>
            @endif
        </div>

        <div class="form-group mt-3">
            <button class="btn btn-primary form-control" type="submit">Сохранить</button>
        </div>
        @include('inc.errors')
    </form>
   </div>
@endsection
@extends('layouts.app')
@section('title')
    Класс {{$group->name}}
@endsection
@section('content')
    {{-- @include('inc.navi') --}}
    <div class="mt-3">
        <h3>{{$group->name}}</h3>
    </div>
    <ul class="nav nav-tabs">
        <li class="active">
          <a data-toggle="tab" href="#students" class="nav-link">Ученики</a>
        </li>
        <li class="active">
          <a data-toggle="tab" href="#tasks" class="nav-link">Задания</a>
        </li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="students">
            <div class="">
                <h3 class="mx-auto">Всего учеников: {{count($students)}}</h3>
                    @if ($students->isNotEmpty())
                        @foreach ($students as $student)
                            <div class="card mt-3">
                                <div class="card-header">
                                    <span>Имя :{{ $student->name }}</span>
                                </div>
                                <div class="card-body">
                                    <span>Всего выполнено заданий: {{$student->solutions->count()}}</span>
                                    <p>Средний балл: {{$student->student_rating}}</p>
                                </div>
                                <div class="card-footer">
                                    <a href="/admin/students/{{$student->id}}" class="btn btn-primary">Решения ученика</a>
                                    <a href="/admin/students/{{$student->id}}/questions" class="btn btn-primary">Вопросы преподавателю</a>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <h3>Учеников еще нет</h3>
                    @endif
                </div>
        </div>
        <div class="tab-pane" id="tasks">
            <div class="">
                <div class="mt-3">
                    <a href="/admin/groups/show/{{$group->id}}/tasks/create" class="float-right btn btn-primary w-25">Добавить задание</a>
                </div>
                <h3 class="mx-auto mt-3">Всего заданий: {{$tasks->count()}}</h3>
                @if ($tasks->isNotEmpty())
                @foreach ($tasks as $task)
                    <div class="card mt-3">
                        <div class="card-header">
                            <span>Название :{{ $task->title }}</span>
                        </div>
                        <div class="card-body">
                            <p>Задание: {{$task->task}}</p>
                            {{-- <span>Выполнило: {{$task->completed}} учеников из {{$tasks->count()}}</span> --}}
                            <a class="btn btn-primary" href="/admin/task/{{$task->id}}">Вопросы учеников</a>
                            <a class="btn btn-primary" href="/admin/task/{{$task->id}}/solutions">Решения учеников</a>
                            <a class="btn btn-primary" href="/admin/task/{{$task->id}}/edit"><i class="fas fa-edit"></i></a>
                            <a class="btn btn-primary" href="/admin/task/{{$task->id}}/delete"><i class="fas fa-trash-alt"></i></a>
                        </div>
                        <div class="card-footer">
                            <span>Крайний срок: {{\Carbon\Carbon::parse($task->deadline)->format('d.m.Y')}}</span>
                        </div>
                    </div>
                @endforeach
            @else
                <h3>Заданий еще нет</h3>
            @endif
            </div>
        </div>
      </div>

    
@endsection
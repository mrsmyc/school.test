@extends('layouts.app')
@section('title')
    Написать обращение в дирекцию
@endsection
@section('content')
    {{-- @include('inc.navi') --}}
    <div>
        <h3>Написать обращение в дирекцию</h3>
        <div class="mt-3">
            <form  action="/student/reports/create" method="POST">
                @csrf
                <div class="form-group">
                    <label for="title">Тема обращения:</label>
                    <input type="text" name="title" id="title" class="form-control">
                </div>

                <div class="form-group">
                    <label for="text">Текст обращения:</label>
                    <textarea class="form-control" name="text" id="text" cols="30" rows="2"></textarea>
                </div>
                
                <div class="form-group mt-3">
                    <button type="submit" class="w-25 btn btn-primary form-control">Отправить</button>
                </div>
            </form>
            @include('inc.errors')
        </div>
    </div>
@endsection
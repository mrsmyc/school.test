@extends('layouts.app')
@section('title')
    Обращения в дирекцию
@endsection
@section('content')
    <div class="mt-3">
        @if (Auth::user()->roles->first()->name == 'student')
            <a class="btn btn-primary" href="/student/reports/create">Написать обращение</a>        
        @else
            <a class="btn btn-primary" href="/teacher/reports/create">Написать обращение</a>                    
        @endif
    </div>
    <div>
        <h3 class="mt-3">Обращения в дирекцию</h3>
        @if ($reports->isEmpty())
            <h4 class="mt-3">У вас еще нет обращений в дирекцию</h4>
        @else
            @foreach ($reports as $report)
                <div class="card mt-3">
                    <div class="card-header">
                        <p>Тема обращения: {{$report->title}}</p>
                    </div>
                    <div class="card-body">
                        <p>Обращение: {{$report->text}}</p>
                    </div>
                    <div class="card-footer">
                        @forelse ($report->answer as $answer)
                            <span class="border-bottom text-warning">Ответ дирекции: </span>
                            <p class="border-bottom mt-2">Тема ответа: {{$answer->title}}</p>
                            <p>Ответ :{{$answer->text}}</p>
                        @empty
                            <p class="text-danger">Ответа еще нет</p>
                        @endforelse
                    </div>
                </div>
            @endforeach
            <div class="mt-3 d-flex justify-content-center">
                {{ $reports->appends(request()->toArray())->links() }}                
            </div>
        @endif  
    </div>
@endsection
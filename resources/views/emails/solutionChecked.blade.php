@component('mail::message')

Ваш ответ на задание {{$task->title}} был проверен и оценен. <br>
Ваша оценка - {{$solution->rating}} <br>
Комментарий учителя - {{$solution->teacher_answer}}

{{--@component('mail::button', ['url' => ''])
Button Text
@endcomponent--}}

Ваш учитель, {{$teacher->name}}

@endcomponent

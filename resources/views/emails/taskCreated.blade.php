@component('mail::message')
{{$author->name}} добавил новое задание для вашей группы! <br>
Заголовок: {{$task->title}} <br>
Задание: {{$task->task}}
@component('mail::button', ['url' => 'http://school.test/student/task/'.$task->id])
Добавить ответ
@endcomponent
@endcomponent

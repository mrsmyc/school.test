@component('mail::message')

Ученик {{$student->name}} дал ответ на задание: <br>
{{$task->title}} <br>
{{$task->task}}

@component('mail::button', ['url' => 'http://school.test/teacher/task/'.$task->id.'/answers'])
Оценить ответ 
@endcomponent



@endcomponent
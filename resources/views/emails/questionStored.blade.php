@component('mail::message')
Ученик {{$student->name}} отправил вам личный вопрос на задание {{$question->task->title}}: <br>
{{$question->text}}
@component('mail::button', ['url' => 'http://school.test/teacher/questions'])
Ответить
@endcomponent

@endcomponent

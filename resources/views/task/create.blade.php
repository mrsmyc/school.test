@extends('layouts.app')
@section('title')
    Добавить задание
@endsection
@section('content')
   <div class="mt-3 w-50 mx-auto">
       @can('admin_properties')
       <form method="POST" action="/admin/groups/show/{id}/tasks/create">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="title">Заголовок:</label>
            <input name="title" id="title" class="form-control" type="text" value="{{ old('name') }}" required>
        </div>
        <input type="text" name="groupId" value="{{$group->id}}" hidden>
        <div class="form-group">
            <label for="task">Задание:</label>
            <textarea class="form-control" name="task" id="task" cols="30" rows="10" value="{{ old('name') }}" required></textarea>
        </div>
        
        <div class="form-group">
            <label for="date">Крайний срок:</label>
            <input type="date" class="form-control" id="date" name="date" required >
        </div>

        <div class="form-group mt-3">
            <button class="btn btn-primary form-control" type="submit">Сохранить</button>
        </div>
        @include('inc.errors')
    </form>
       @elsecan('teacher_properties')
       <form method="POST" action="/teacher/group/{id}/tasks/create">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="title">Заголовок:</label>
            <input name="title" id="title" class="form-control" type="text" value="{{ old('name') }}" required>
        </div>
        <input type="text" name="groupId" value="{{$group->id}}" hidden>
        <div class="form-group">
            <label for="task">Задание:</label>
            <textarea class="form-control" name="task" id="task" cols="30" rows="10" value="{{ old('name') }}" required></textarea>
        </div>
        
        <div class="form-group">
            <label for="date">Крайний срок:</label>
            <input type="date" class="form-control" id="date" name="date" required >
        </div>

        <div class="form-group mt-3">
            <button class="btn btn-primary form-control" type="submit">Сохранить</button>
        </div>
        @include('inc.errors')
    </form>
       @endcan
    
   </div>
@endsection
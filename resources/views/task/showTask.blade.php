@extends('layouts.app')
@section('title')
    Задание {{$task->title}}
@endsection
@section('content')
    <div>
        <h3>Заголовок задания: {{$task->title}}</h3>
    </div>
    <div>
        <h3>Задание: {{$task->task}}</h3>
    </div>
    <ul class="nav nav-tabs">
        <li class="nav-item">
          <a class="nav-link active" data-toggle="tab" href="#solution">Решение</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#comments">Комментарии</a>
        </li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane fade show active" id="solution">
            <div>
                @if ($solution==null)
                    <div class="card">
                        <div class="card-body">
                            <h5>Вы еще не выполнили это задание!</h5>
                            <a href="/student/task/{{$task->id}}/solution/add">Добавить ответ</a>
                        </div>
                    </div>
                @else
                <h5 class="mt-3">Ваш ответ на данное задание:</h5>
                    <div class="card">
                        <div class="card-body">
                            <p>{{$solution->text}}</p>
                        </div>
                        <div class="card-footer">
                            @if ($solution->teacher_anwser==null && $solution->rating==null)
                                <p>Учитель еще не проверил ваш ответ</p>
                                <a href="/student/task/{{$solution->student->id}}/solution/{{$solution->id}}/edit" class="btn btn-primary">Редактировать ответ</a>
                            @else
                                <p>Ваша оценка: {{$solution->rating}}</p>
                                <p>Комментарий учителя к вашей работе: {{$solution->teacher_answer}}</p>                                                
                            @endif
                        </div>
                    </div>
                @endif
            </div>
          </div>
        <div class="tab-pane fade" id="comments">
            <div>
                @if ($comments->isEmpty())
                    <h5 class="mt-2">Комментариев еще нет</h5>
                @else
                    <h5 class="mt-2">Комментарии: </h5>
                    @foreach ($comments as $comment)
                        <div class="card mt-3">
                            <div class="card-body">
                                <p>{{$comment->text}}</p>
                                <span class="border-top">Автор: {{$comment->student->name}}</span>
                            </div>
                            
                            <div class="card-footer">
                                
                                @forelse ($comment->answer as $answer)
                                    <p>Ответ учителя: {{$answer->answer}}</p>                                                        
                                @empty
                                    <span class="text-danger">Учитель еще не ответил</span>                            
                                @endforelse
        
                            </div>
                        </div>
                    @endforeach
                @endif
                <h4>Оставить комментарий:</h4>
                <div>
                    <form action="/student/task/{{$task->id}}" method="POST">
                        {{-- <input type="text" name="taskId" value="" hidden> --}}
                        @csrf
                        <div class="form-group">
                            <textarea class="form-control" name="comment" id="" cols="30" rows="2"></textarea>
                        </div>
                        
                        <div class="form-group">
                            <button class="w-25 mt-3 form-control btn btn-primary" type="submit">Отправить</button>
                        </div>                
                    </form>
                </div>
                @include('inc.errors')
            </div>
          </div>
      </div>

   
   
@endsection
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    {{-- <a class="navbar-brand ml-5" href="/">School</a> --}}
    @if (Auth::check())
    @if (Auth::user()->roles->first()->name == 'admin')
      <a class="navbar-brand ml-5" href="/admin/groups">School</a>  
    @elseif(Auth::user()->roles->first()->name == 'teacher')
      <a class="navbar-brand ml-5" href="/teacher/group">School</a>  
    @else
      <a class="navbar-brand ml-5" href="/student/tasks">School</a>  
    @endif
    @endif
    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarsExample04" aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  
    <div class="navbar-collapse collapse" id="navbarsExample04" style="">
      
      <ul class="navbar-nav mx-auto mr-5">
        @can('admin_properties')
                {{-- <li class="nav-item ">
                    <a href="/admin/groups" class="nav-link">Классы</a>
                </li> --}}
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Классы</a>
                   <div class="dropdown-menu" aria-labelledby="dropdown04">
                     <a class="dropdown-item" href="/admin/groups">Все классы</a>
                     <a class="dropdown-item" href="/admin/groups/create">Добавить класс</a>
                   </div>
                 </li>
                {{-- <li class="nav-item ">
                    <a href="/admin/teachers" class="nav-link">Учителя</a>
                </li>--}}
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Учителя</a>
                   <div class="dropdown-menu" aria-labelledby="dropdown04">
                     <a class="dropdown-item" href="/admin/teachers">Все учителя</a>
                     <a class="dropdown-item" href="/admin/teachers/create">Добавить учителя</a>
                   </div>
                 </li>
                {{-- <li class="nav-item ">
                    <a href="/admin/students" class="nav-link">Ученики</a>
                </li> --}}
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Ученики</a>
                   <div class="dropdown-menu" aria-labelledby="dropdown04">
                     <a class="dropdown-item" href="/admin/students">Все ученики</a>
                     <a class="dropdown-item" href="/admin/students/create">Добавить ученика</a>
                   </div>
                 </li>
                <li class="nav-item ">
                    <a href="/admin/reports" class="nav-link">Обращения</a>
                </li>   
            @endcan
            @can('student_properties')
                <li class="nav-item ">
                    <a href="/student/tasks" class="nav-link">Мои задания</a>
                </li>
                <li class="nav-item ">
                    <a href="/student/marks" class="nav-link">Мои оценки</a>
                </li>
                <li class="nav-item ">
                    <a href="/student/reports" class="nav-link">Обращения в дирекцию</a>
                </li>
            @endcan
            @can('teacher_properties')
                <li class="nav-item ">
                    <a href="/teacher/group" class="nav-link">Мой класс</a>
                </li>
                {{-- <li class="nav-item dropdown">
                    <a href="/teacher/group/{{$group->id ?? ''}}/tasks" class="nav-link">Задания</a>
                    <div class="dropdown-menu ">
                      <a href="" class="dropdown-item">Создать </a>
                    </div>
                </li> --}}
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Задания</a>
                   <div class="dropdown-menu" aria-labelledby="dropdown04">
                     <a class="dropdown-item" href="/teacher/group/{{$group->id ?? ''}}/tasks">Все задания</a>
                     <a class="dropdown-item" href="/teacher/group/{{Auth::user()->teacher_group_id}}/tasks/create">Добавить задание</a>
                   </div>
                 </li>
         
                <li class="nav-item ">
                    <a href="/teacher/questions" class="nav-link">Личные вопросы от учеников</a>
                </li>
                <li class="nav-item ">
                    <a href="/teacher/reports" class="nav-link">Обращения в дирекцию</a>
                </li>
            @endcan
        @if (Auth::check())
        <li class="nav-item">
          <a class="nav-link" href="/logout">Выйти</a>
        </li>
         @else
        <li class="nav-item">
        <a class="nav-link" href="/login">Войти</a>
        </li>
        @endif         
      </ul>
      
      
    </div>
  </nav>
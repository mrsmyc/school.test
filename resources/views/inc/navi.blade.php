<nav class="navbar navbar-expand-md ">
    <div class="row navbar-collapse">
        <ul class="navbar-nav">
            @can('admin_properties')
                {{-- <li class="nav-item mx-auto">
                    <a href="/admin/groups" class="nav-link">Классы</a>
                </li>
                <li class="nav-item mx-auto">
                    <a href="/admin/teachers" class="nav-link">Учителя</a>
                </li>                        
                <li class="nav-item mx-auto">
                    <a href="/admin/students" class="nav-link">Ученики</a>
                </li>
                <li class="nav-item mx-auto">
                    <a href="/admin/reports" class="nav-link">Обращения</a>
                </li>    --}}
                <li class="nav-item mx-auto">
                    <a href="/test/1task" class="nav-link">1 задание</a>
                </li>
                <li class="nav-item mx-auto">
                    <a href="/test/2task" class="nav-link">2 задание</a>
                </li>
                <li class="nav-item mx-auto">
                    <a href="/test/3task" class="nav-link">3 задание</a>
                </li>
                <li class="nav-item mx-auto">
                    <a href="/test/4task" class="nav-link">4 задание (округление цены)</a>
                </li>
            @endcan
            @can('student_properties')
                <li class="nav-item mx-auto">
                    <a href="/student/tasks" class="nav-link">Мои задания</a>
                </li>
                <li class="nav-item mx-auto">
                    <a href="/student/marks" class="nav-link">Мои оценки</a>
                </li>
                {{-- <li class="nav-item mx-auto">
                    <a href="" class="nav-link">Общение с учителем</a>
                </li> --}}
                <li class="nav-item mx-auto">
                    <a href="/student/reports" class="nav-link">Обращения в дирекцию</a>
                </li>
            @endcan
            @can('teacher_properties')
                <li class="nav-item mx-auto">
                    <a href="/teacher/group" class="nav-link">Мой класс</a>
                </li>
                <li class="nav-item mx-auto">
                    <a href="/teacher/group/{{$group->id ?? ''}}/tasks" class="nav-link">Задания</a>
                </li>
                <li class="nav-item mx-auto">
                    <a href="/teacher/questions" class="nav-link">Личные вопросы от учеников</a>
                </li>
                <li class="nav-item mx-auto">
                    <a href="/teacher/reports" class="nav-link">Обращения в дирекцию</a>
                </li>
            @endcan
            </ul>
    </div>
</nav>
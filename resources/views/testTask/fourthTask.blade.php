@extends('layouts.app')
@section('content')
    <h1>Четвертое задание - округлить цену</h1>
    <form action="/test/4task" method="POST">
        @csrf
        <div class="form-group">
            <label for="number">цена</label>
            <input type="number" name="number" id="number" class="form-control">
        </div>
        <div class="form-group">
            <label for="loss">допустимый убыток</label>
            <input type="number" name="loss" id="loss" class="form-control">
        </div>
        <div class="form-group">
            <button class="btn btn-primary">Округлить</button>
        </div>
    </form>
@endsection
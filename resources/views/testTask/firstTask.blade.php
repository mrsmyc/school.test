@extends('layouts.app')
@section('content')
    <h1>Первое задание - из буквы получить число</h1>
    <form action="/test/1task" method="POST">
        @csrf
        <div class="form-group">
            <label for="letter">буква</label>
            <input type="text" name="letter" id="letter" class="form-control">
        </div>
        <div class="form-group">
            <button class="btn btn-primary">Проверить</button>
        </div>
    </form>
@endsection
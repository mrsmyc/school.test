@extends('layouts.app')
@section('content')
    <h1>Второе задание - числа букву</h1>
    <form action="/test/2task" method="POST">
        @csrf
        <div class="form-group">
            <label for="number">число</label>
            <input type="number" name="number" id="number" class="form-control">
        </div>
        <div class="form-group">
            <button class="btn btn-primary">Проверить</button>
        </div>
    </form>
@endsection
@extends('layouts.app')
@section('content')
    <h1>Третье задание - сдвиг буквы на n столбцов</h1>
    <form action="/test/3task" method="POST">
        @csrf
        <div class="form-group">
            <label for="letter">буква</label>
            <input type="text" name="letter" id="letter" class="form-control">
        </div>
        <div class="form-group">
            <label for="number">число</label>
            <input type="number" name="number" id="number" class="form-control">
        </div>
        <div class="form-group">
            <button class="btn btn-primary">Проверить</button>
        </div>
    </form>
@endsection
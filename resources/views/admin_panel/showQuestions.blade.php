@extends('layouts.app')
@section('title')
    Вопросы ученика
@endsection
@section('content')
    {{-- @include('inc.navi') --}}
    @if($questions->isEmpty()) 
    @else 
        <h3 class="mt-3">Вопросы ученика: {{$questions->first()->student->name}}</h3>
        <h3 class="">Всего вопросов: {{$questions->total()}}</h3>
    @endif
    <div class="mt-3">
        @if ($questions->isNotEmpty())
            @foreach ($questions as $question)
                <div class="card mt-3">
                    <div class="card-header">
                        <p>Вопрос к заданию: {{$question->task->title}}</p>
                    </div>
                    <div class="card-body">
                        <p>Вопрос: <span class="text-success">{{$question->text}}</span></p>
                        @forelse ($question->answer as $answer)
                            <p>Ответ учителя: <span class="text-warning">{{$answer->text}}</span></p>
                        @empty
                            <p>Учитель еще не ответил</p>
                        @endforelse
                        <a class="btn btn-primary" href="/admin/students/{{$question->student->id}}/questions/{{$question->id}}"><i class="fas fa-edit"></i></a>
                        <a class="btn btn-primary" href="/admin/students/{{$question->student->id}}/questions/{{$question->id}}/delete"><i class="fas fa-trash-alt"></i></a>
                        {{-- <a href="/admin/groups/{{$group->id}}/teacher/change" class="ml-3">Сменить учителя</a> --}}
                    </div>
                </div>
            @endforeach
            <div class="mt-3 d-flex justify-content-center">
                {{ $questions->appends(request()->toArray())->links() }}                
            </div>
        @else
            <h3>Вопросов еще нет</h3>
        @endif
    </div>

@endsection
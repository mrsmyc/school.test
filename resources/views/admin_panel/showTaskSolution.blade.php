@extends('layouts.app')
@section('title')
    Решения
@endsection
@section('content')
    {{-- @include('inc.navi') --}}
    <div class="mt-3">
        @if ($solutions->isEmpty())
        <h3>Ни один ученик не выполнил задание</h3>        
        @else
            <h3>Решения учеников: </h3>
            @foreach ($solutions as $solution)
                <div class="card mt-3">
                    <div class="card-header">
                        <p>Предмет задания: {{$solution->task->title}}</p>
                        <p class="border-top">Задание: {{$solution->task->task}}</p>
                    </div>
                    <div class="card-body">
                        <p>Решение: <span class="text-success">{{$solution->text}}</span></p>
                        @if ($solution->rating == null && $solution->teacher_answer == null)
                            <p class="text-danger">Работа еще не проверена</p>
                        @else
                            <p>Ответ учителя: <span class="text-warning">{{$solution->teacher_answer}}</span></p>
                            <p>Оценка: {{$solution->rating}}</p>
                        @endif
                        <p>Автор ответа: {{$solution->student->name}}</p>
                        <a href="/admin/task/{{$solution->task->id}}/solutions/{{$solution->id}}/redact" class="btn btn-primary"><i class="fas fa-edit"></i></a>
                        {{-- <a href="/admin/students/{{$student->id}}/solution/{{$solution->id}}/redact" class="btn btn-primary">Редактировать решение</a> --}}
                    </div>
                    <div class="card-footer">
                        <p>Крайний срок: {{\Carbon\Carbon::parse($solution->task->deadline)->format('d|m|Y')}}</p>
                    </div>
                </div>
            @endforeach
            <div class="mt-3 d-flex justify-content-center">
                {{ $solutions->appends(request()->toArray())->links() }}                
            </div>
        @endif
    </div>
    
    
@endsection
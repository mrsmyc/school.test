@extends('layouts.app')
@section('title')
    Редактирование вопроса ученика
@endsection
@section('content')
@include('inc.navi')
   <div class="mt-3 w-50 mx-auto">
       <h3>Редактирование вопроса от ученика: {{$comment->student->name}} на задание {{$task->title}}</h3>
    <form method="POST" action="/admin/task/{{$task->id}}/comment/edit/{{$comment->id}}">        
        @csrf
        <div class="form-group">
            <label for="comment">Комментарий:</label>
            <input name="comment" id="comment" class="form-control" type="text" value="{{$comment->text}}" required>
        </div>

        <div class="form-group">
            <label for="answer">Ответ учителя:</label>
            <textarea class="form-control" name="answer" id="answer" cols="30" rows="10"  required>@if ($commentAnswer){{ $commentAnswer->answer }}
            @else @endif</textarea>
        </div>

        <div class="form-group mt-3">
            <button class="btn btn-primary form-control" type="submit">Сохранить</button>
        </div>
        @include('inc.errors')
    </form>
   </div>
@endsection
@extends('layouts.app')
@section('title')
    Редактирование решения 
@endsection
@section('content')
{{-- @include('inc.navi') --}}
   <div class="mt-3 w-50 mx-auto">
       <h3>Редактирование решения ученика ({{$student->name}}) задания "{{$solution->task->title}}" </h3>
    <form method="POST" action="/admin/students/{{$student->id}}/solution/{{$solution->id}}/redact">        
        @csrf
        <div class="form-group">
            <label for="text">Решение:</label>
            <textarea class="form-control" name="text" id="text" cols="30" rows="4" required>{{$solution->text}}</textarea>
            {{-- <input name="text" id="text" class="form-control" type="text" value="{{$solution->text}}" required> --}}
        </div>

        <div class="form-group">
            <label for="answer">Ответ учителя:</label>
            <textarea class="form-control" name="answer" id="answer" cols="30" rows="10"  required>{{ $solution->teacher_answer }}</textarea>
        </div>
        
        <div class="form-group">
            <label for="rating">Оценка:</label>
            <input type="number" min="1" max="5" class="form-control" id="rating" name="rating"  value="{{$solution->rating}}">
        </div>

        <div class="form-group mt-3">
            <button class="btn btn-primary form-control" type="submit">Сохранить</button>
        </div>
        @include('inc.errors')
    </form>
   </div>
@endsection
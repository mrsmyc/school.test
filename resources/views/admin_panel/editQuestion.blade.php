@extends('layouts.app')
@section('title')
    Редактирование вопрос 
@endsection
@section('content')
{{-- @include('inc.navi') --}}
   <div class="mt-3 w-50 mx-auto">
       <h3>Редактирование вопроса {{$question->student->name}} учитлю на задание | {{$question->task->title}}</h3>
    <form method="POST" action="/admin/students/{{$question->student->id}}/questions/{{$question->id}}">        
        @csrf
        <div class="form-group">
            <label for="text">Вопрос:</label>
            @if ($question)
                <textarea class="form-control" name="text" id="text" cols="30" rows="4" required>{{$question->text}}</textarea>                
            @else
                <textarea class="form-control" name="text" id="text" cols="30" rows="4" required></textarea>                
            @endif
        </div>

        <div class="form-group">
            <label for="answer">Ответ учителя:</label>
            @if ($answer)
                <textarea class="form-control" name="answer" id="answer" cols="30" rows="10"  required>{{$answer->text}}</textarea>                
            @else
                <textarea class="form-control" name="answer" id="answer" cols="30" rows="10"  required></textarea>                                
            @endif
        </div>        

        <div class="form-group mt-3">
            <button class="btn btn-primary form-control" type="submit">Сохранить</button>
        </div>
        @include('inc.errors')
    </form>
   </div>
@endsection
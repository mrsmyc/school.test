@extends('layouts.app')
@section('title')
    Ответить на обращение
@endsection
@section('content')
    
    <div>
        <h3>Ответить на обращение {{$report->author->name}} </h3>
    </div>
    <div class="card">
        <div class="card-header">
            <p>Тема обращения: {{$report->title}}</p>
        </div>
        <div class="card-body">
            <p>{{$report->text}}</p>
        </div>
        <div class="card-footer">
            <p>Автор: {{$report->author->name}}</p>
            <p>Дата: {{$report->created_at}}</p>
        </div>
    </div>
    <div>
        <form action="/admin/reports/{{$report->id}}/answer" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Тема ответа:</label>
                <input type="text" class="form-control" name="title" id="title" required>
            </div>

            <div class="form-group">
                <label for="text">Ответ:</label>
                <textarea name="text" id="text" cols="30" rows="4" class="form-control" required></textarea>
            </div>

            <div class="form-group">
                <button type="submit" class="w-25 mt-3 btn btn-primary form-control">Ответить</button>
            </div>
        </form>
    </div>
    
    
@endsection
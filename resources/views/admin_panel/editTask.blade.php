@extends('layouts.app')
@section('title')
    Редактирование задания: {{$task->title}}
@endsection
@section('content')
{{-- @include('inc.navi') --}}
   <div class="mt-3 w-50 mx-auto">
       <h3>Редактирование задания "{{$task->title}}" для класса: {{$task->group->name}}</h3>
    <form method="POST" action="/admin/task/{{$task->id}}/edit">        
        @csrf
        <div class="form-group">
            <label for="title">Заголовок:</label>
            <input name="title" id="title" class="form-control" type="text" value="{{$task->title}}" required>
        </div>

        <div class="form-group">
            <label for="task">Задание:</label>
            <textarea class="form-control" name="task" id="task" cols="30" rows="10"  required>{{ $task->task }}</textarea>
        </div>
        
        <div class="form-group">
            <label for="date">Дата:</label>
            <input type="date" class="form-control" id="date" name="date" required value="{{$task->date}}">
        </div>

        <div class="form-group mt-3">
            <button class="btn btn-primary form-control" type="submit">Сохранить</button>
        </div>
        @include('inc.errors')
    </form>
   </div>
@endsection
@extends('layouts.app')
@section('title')
    Ученик {{$student->name}}
@endsection
@section('content')
    {{-- @include('inc.navi') --}}
    <div class="mt-3">
        <h3>Ученик: {{$student->name}}</h3>
    </div>
    <div>
        @if ($solutions->isEmpty())
        <h3>Ученик еще не решил ни одно задание</h3>        
        @else
            <h3>Всего ответов ученика: {{$solutions->total()}} </h3>
            @foreach ($solutions as $solution)
                <div class="card mt-3">
                    <div class="card-header">
                        <p>Предмет задания: {{$solution->task->title}}</p>
                        <p class="border-top">Задание: {{$solution->task->task}}</p>
                    </div>
                    <div class="card-body">
                        <p>Решение: <span class="text-success">{{$solution->text}}</span></p>
                        <p>Ответ учителя: <span class="text-warning">{{$solution->teacher_answer}}</span></p>
                        <p>Оценка: {{$solution->rating}}</p>
                        <a href="/admin/students/{{$student->id}}/solution/{{$solution->id}}/redact" class="btn btn-primary"><i class="fas fa-edit"></i></a>
                        <a href="/admin/students/{{$student->id}}/solution/{{$solution->id}}/delete" class="btn btn-primary"><i class="fas fa-trash-alt"></i></a>
                    </div>
                    <div class="card-footer">
                        <p>Крайний срок: {{\Carbon\Carbon::parse($solution->task->deadline)->format('d.m.Y')}}</p>
                    </div>
                </div>
            @endforeach
            <div class="mt-3 d-flex justify-content-center">
                {{ $solutions->appends(request()->toArray())->links() }}                
            </div>
        @endif
    </div>
    
    
@endsection
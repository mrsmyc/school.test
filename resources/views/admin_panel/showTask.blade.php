@extends('layouts.app')
@section('title')
    Задание {{$task->title}}
@endsection
@section('content')
    {{-- @include('inc.navi') --}}
    <div class="mt-3">
        <h3>Заголовок задания: {{$task->title}}</h3>
        <h3>Задание: {{$task->task}}</h3>
    </div>
    <div class="mt-2">
        @if ($comments->isEmpty())
        <h3>Ученики еще не задавали вопросов по поводу задания</h3>        
        @else
            <h3>Вопросы учеников: </h3>
            @foreach ($comments as $comment)
                <div class="card mt-3">
                    <div class="card-header">
                        <p>Автор: {{$comment->student->name}}</p>
                    </div>
                    <div class="card-body">
                        <p>Вопрос ученика: {{$comment->text}}</p>
                        <a href="/admin/task/{{$task->id}}/comment/edit/{{$comment->id}}" class="btn btn-primary"><i class="fas fa-edit"></i></a>
                        <a href="/admin/task/{{$task->id}}/comment/{{$comment->id}}/delete" class="btn btn-primary"><i class="fas fa-trash-alt"></i></a>                        
                    </div>
                    <div class="card-footer">
                        @forelse ($comment->answer as $answer)
                            <p>Ответ учителя: {{$answer->answer}}</p>
                            <p>Учитель: {{$answer->author->name}}</p>                            
                        @empty
                            <p>Ответа от учителя еще нет</p>
                        @endforelse
                    </div>
                </div>
            @endforeach
            <div class="mt-3 d-flex justify-content-center">
                {{ $comments->appends(request()->toArray())->links() }}                
            </div>
        @endif
    </div>
    
    
@endsection
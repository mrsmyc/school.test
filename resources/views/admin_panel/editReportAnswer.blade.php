@extends('layouts.app')
@section('title')
    Редактировать ответ
@endsection
@section('content')
    
    <div>
        <h3>Редактировать ответ на обращение {{$answer->report->title}} </h3>
    </div>
    <div class="card">
        <div class="card-header">
            <p>Тема обращения: {{$answer->report->title}}</p>
        </div>
        <div class="card-body">
            <p>{{$answer->report->text}}}</p>
        </div>
        <div class="card-footer">
            <p>Автор: {{$answer->report->author->name}}</p>
        </div>
    </div>
    <div>
        <form action="/admin/reports/{{$answer->report->id}}/answer/{{$answer->id}}/redact" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Тема ответа:</label>
                <input value="{{$answer->title}}" type="text" class="form-control" name="title" required id="title">
            </div>

            <div class="form-group">
                <label for="text">Ответ:</label>
                <textarea name="text" id="text" cols="30" rows="4" class="form-control" required>{{$answer->text}}</textarea>
            </div>

            <div class="form-group">
                <button type="submit" class="w-25 mt-3 btn btn-primary form-control">Ответить</button>
            </div>
        </form>
    </div>
    
    
@endsection
@extends('layouts.app')
@section('title')
    Обращения
@endsection
@section('content')
    {{-- @include('inc.navi') --}}
    <div class="mt-3">
        <h3>Всего обращений: {{$reports->total()}}</h3>
    </div>
    <div>
        @if ($reports->isEmpty())
        <h3>Обращений еще нет</h3>        
        @else
            <h3>Обращения: </h3>
            @foreach ($reports as $report)
                <div class="card mt-3">
                    <div class="card-header">
                        <p>Автор: {{$report->author->name}}</p>
                        <p class="border-top">Предмет обращения: {{$report->title}}</p>
                        <p class="border-top">Обращение: {{$report->text}}</p>
                    </div>
                    <div class="card-body">
                        @forelse ($report->answer as $answer)
                            <p>Предмет ответа: <span class="text-warning">{{$answer->title}}</span></p>
                            <p>Ваш ответ: <span class="text-success">{{$answer->text}}</span></p>
                            <a href="/admin/reports/{{$report->id}}/answer/{{$answer->id}}/redact" class="btn btn-primary"><i class="fas fa-edit"></i></a>
                        @empty
                            <p class="text-danger">Ответа еще нет</p>
                            <a href="/admin/reports/{{$report->id}}/answer" class="btn btn-primary">Ответить</a>
                        @endforelse
                            <a href="/admin/reports/{{$report->id}}/delete" class="btn btn-primary"><i class="fas fa-trash-alt"></i></a>
                    </div>
                    <div class="card-footer">
                        <p>Дата обращения: {{\Carbon\Carbon::parse($report->created_at)->format('d.m.Y')}}</p>
                    </div>
                </div>
            @endforeach
            <div class="mt-3 d-flex justify-content-center">
                {{ $reports->appends(request()->toArray())->links() }}                
            </div>
        @endif
    </div>
    
    
@endsection
@extends('layouts.app')
@section('title')
    Все ученики
@endsection
@section('content')
    {{-- @include('inc.navi') --}}
    <div class="mt-3">
        <a href="/admin/students/create" class="btn btn-primary w-25">Добавить ученика</a>
    </div>
    <h3 class="mt-3">Всего учеников: {{$students->total()}}</h3>
    <div class="mt-3">
        @if ($students->isNotEmpty())
            @foreach ($students as $student)
                <div class="card mt-3">
                    <div class="card-header">
                        <span>Имя :{{ $student->name }}</span>
                    </div>
                    <div class="card-body">
                        @if (!$student->groupAsStudent)
                           <h5>Класс: еще не назначен</h5>
                           <a href="/admin/students/{{$student->id}}/enroll">Зачислить в класс</a>                             
                        @else
                           <a href="/admin/groups/show/{{$student->student_group_id}}">Класс: {{ $student->groupAsStudent->name }}</a>
                            <p>Средний балл: {{$student->student_rating}}</p>
                            <p>Всего решений: {{$student->solutions->count()}}</p>
                            {{-- <a class="btn btn-primary" href="/admin/groups/show/{{$student->student_group_id}}">Посмотреть класс</a>  --}}
                            {{-- <a href="" class="btn btn-primary">Перевести в другой класс</a>  --}}
                            <a href="/admin/students/{{$student->id}}" class="btn btn-primary">Решения ученика</a>
                            <a href="/admin/students/{{$student->id}}/questions" class="btn btn-primary">Вопросы преподавателю</a>                           
                        @endif
                    </div>
                </div>
            @endforeach
        @else
            <h3>Учеников еще нет</h3>
        @endif
    </div>
    <div class="mt-3">
        {{ $students->appends(request()->toArray())->links() }}
    </div>
@endsection
@extends('layouts.app')
@section('title')
    Зачислить ученика в класс
@endsection
@section('content')
    <div class="mt-3">
        <p>Зачислить {{$student->name}}</p>
    </div>
    <form action="/admin/students/{id}/enroll" method="POST">
        @csrf
        <div class="form-group">
            <input type="text" name="studentId" value="{{$student->id}}" hidden>
            <label for="group">Выберите класс</label>
            <select name="group" id="group" class="form-control">
                @foreach ($groups as $group)
                    <option value="{{$group->id}}">{{$group->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group mt-3">
            <button class="btn btn-primary" type="submit">Зачислить</button>
        </div>
    </form>
    
@endsection
@extends('layouts.app')
@section('title')
    Все учителя
@endsection
@section('content')
    {{-- @include('inc.navi') --}}
    <div class="mt-3">
        <a href="/admin/teachers/create" class="btn btn-primary w-25">Добавить учителя</a>
    </div>
    <h3 class="mt-3">Всего учителей: {{$teachers->total()}}</h3>
    <div class="mt-3">
        @if ($teachers->isNotEmpty())
            @foreach ($teachers as $teacher)
                <div class="card mt-3">
                    <div class="card-header">
                        <span>Имя : {{ $teacher->name }}</span>
                    </div>
                    <div class="card-body">
                        @if (!$teacher->groupAsTeacher)
                           <h4>Класс: еще не назначен</h4>
                           <a href="/admin/groups/create">Создать класс</a>                             
                        @else
                           <h4>Класс: {{ $teacher->groupAsTeacher->name }}</h4>
                            <a href="/admin/groups/show/{{$teacher->teacher_group_id}}">Посмотреть класс</a>                             
                        @endif
                        
                    </div>
                </div>
            @endforeach
        @else
            <h3>Учителей еще нет</h3>
        @endif
    </div>
    <div class="mt-3">
        {{ $teachers->appends(request()->toArray())->links() }}
    </div>
@endsection
@extends('layouts.app')
@section('title')
    Добавить учителя
@endsection
@section('content')
   <div class="mt-3 w-50 mx-auto">
    <form method="POST" action="/admin/teachers/create">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name">Введите имя учителя:</label>
            <input name="name" id="name" class="form-control" type="text" value="{{ old('name') }}" required>
        </div>
        
        <div class="form-group">
            <label for="email">Введите email:</label>
            <input name="email" id="email" class="form-control" type="email" value="{{ old('name') }}" required>
        </div>
        
        <div class="form-group">
            <label for="password">Пароль:</label>
            <input type="password" class="form-control" id="password" name="password" required >
        </div>

        <div class="form-group">
            <label for="password_confirmation">Пароль еще раз:</label>
            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" required>
        </div>

        

        <div class="form-group mt-3">
            <button class="btn btn-primary form-control" type="submit">Сохранить</button>
        </div>
        @include('inc.errors')
    </form>
   </div>
@endsection